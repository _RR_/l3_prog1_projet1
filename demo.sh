#!/bin/sh

# Basic demo
#Clear the command console.
clear

echo "		Demonstration of Project-1"
echo "by Théo LOSEKOOT, Rémi PIAU and Kerian THUILLIER"

cd src

#Clear compilation files
echo "\nClean the compilation files."
sh clean.sh

#Compile files
echo "\nCompile needed files."
sh compile.sh

echo "\n\n\n"

echo "Start main demo"
#Start the demonstration script
ocaml demo.ml

echo "\n\nStart additional script"
ocaml demo_3dsurface.ml

echo "\nStart the user interface"
ocaml demo_render.ml
echo "\nEnd of the demonstration."

# FOR A MORE ADVANCED SCRIPT (YOU CAN PASS ARGS)
#if [ $# -eq 1 ]
#then
#    cd code
#    make
#    ocaml code/penrose_base.ml $1
#    make clean
#else
#    echo "Usage : $0 <number_of_iterations>"
#fi
