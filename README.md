# L3_prog1_projet1: Triangulation de Delaunay
Triangulation de Delaunay by Losekoot/Piau/Thuillier

This project is the implementation of the Delaunay triangulation, a 
plane triangulation such as the only points inside the circumscribed 
circle of each triangle are the triangle vertex.
To complete this subject, our team also developed gradient 
coloring and 3d surface analysis functions.


## Requirements
To properly run the scripts, you will need:
* a unix system with bash/sh and the basic unix tools.
* the latest version of OCaml (with graphics and unix included).

## Instructions

### The Demo
Run the **full featured** demo with
```
./demo.sh
```

---

### Compile / Clean
To compile/clean manually:
1. place yourself in the source (`src/`) directory (a `cd src` will do).
2. run `sh compile.sh` to compile, `sh clean.sh` to clean directories.

### Run

#### Tests
To run the module's tests:
1. place yourself in the source directory (a `cd src` will do).
2. be sure to have compiled.
3. run `ocaml tests/test_name.ml` with `test_name.ml` the name of a test file.

#### Demos
To run one of the __specific__ demos, please do the following:
1. place yourself in the source directory (a `cd src` will do).
2. be sure to have compiled.
3. run:
   * `ocaml demo_render.ml` for the sandbox mode.
   * `ocaml demo_3dsurface.ml` for the coloring based on 3d surfaces addon.

### Main functions prototypes

Apply Delaunay triangulation on a set of points given
a frame (form (0,0) to (xmax,ymax)), return a triangle set.
```ocaml
delaunay : Point.point Aset.aset -> int -> int -> Triangle.triangle Aset.aset
```

Add a point to a given triangulation.
```ocaml
add_point : Triangle.triangle Aset.aset -> Point.point -> Triangle.triangle Aset.aset
```

Apply Delaunay triangulation on a set of points given
a frame (form (0,0) to (xmax,ymax)), and display the construction process.
```ocaml
delaunay_stepwise : Point.point Aset.aset -> int -> int -> unit
```
---

Global Note: *be sure* to be in the directory before running the scripts
as they have no awareness of where they are launched from.

