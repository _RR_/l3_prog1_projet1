(** Define a mesh with :
    	- a point : point, representing its origin.
    	- a step : float, the space between two points of the mesh.
    	- a offset : float, the size of a side the mesh.
    	- a length : int, number of step possible with the offset.
    	- a function : (float -> float -> float), function computing
    z value thanks to x and y.
 **)
type mesh = 
  (Point.point * float * float *  int * (float -> float -> float));;

(** Return the origin of a mesh, point.
    	- amesh : mesh.
 **)
let get_start_point amesh =
  let p, _, _, _, _ = amesh in
  p;;

(** Return the step of a mesh, point.
    	- amesh : mesh.
 **)
let get_step amesh =
  let _, step, _, _, _ = amesh in
  step;;

(** Return the offset of a mesh, point.
    	- amesh : mesh.
 **)
let get_offset amesh =
  let _, _, offset, _, _ = amesh in
  offset;;

(** Return the length of a mesh, point.
    	- amesh : mesh.
 **)
let get_length amesh =
  let _, _, _, l, _ = amesh in
  l;;

(** Return the associated function of a mesh, point.
    	- amesh : mesh.
 **)  
let get_fun amesh =
  let _, _, _, _, f = amesh in
  f;;

(** Build a squared mesh from a point 'p' to 'p + (offset, offset)'
    whith the specified steps and its associated function.
    	- p : point.
    	- offset : float.
    	- step : float.
    	- afun : (float -> float -> float), associated function.
 **)
let new_mesh p offset step afun =
  let x, y = 
    Point.float_coord_of p
  in
  let length = 
    int_of_float ((x +. offset) /. step)
  in
  (p, step, offset, length, afun);;
