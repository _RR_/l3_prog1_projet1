#directory "build/";;
#directory "../../build/";;

#load "structure.cma";;
#load "3dsurface.cma";;
open Mesh;;

(** Return the z value of a point according to the mesh's function.
    	- amesh : mesh.
    	- p : point.
 **)
let get_z amesh p = 
  let x,y = 
    Point.float_coord_of (Point.sub p (get_start_point amesh))
  and f = 
    get_fun amesh
  in
  f x y;;

(** Return a point set containing each elements of the initial set which
    are valid points. A valid point is a point (x, y) such as : x > 0 &&
    y > 0.
    	- mesh : mesh.
    	- pt_set : point aset.
 **)
let clean_point_set mesh pt_set =
  Aset.filter 
    pt_set
    (fun p ->
       let x, y = 
         Point.float_coord_of (get_start_point mesh)
       and offset = 
         get_offset mesh 
       in
       let px, py = 
         Point.float_coord_of p
       and max_x = 
         x +. offset
       and max_y = 
         y +. offset 
       in
       (px >= x && py >= y && px <= max_x && py <= max_y)
    );;

(** Return a point set containing each elements of the initial set which
    are valid points. A valid point is a point (x, y) such as : x > 0 &&
    y > 0.
    	- tr_set : triangle aset.
    	- max_x : float, maximum x coordinate of the triangulation.
    	- max_y : float, maximum y coordinate of the triangulation.	
 **)
let clean_triangle_set tr_set max_x max_y =
  let p0 = Point.make 0.0 0.0
  and p1 = Point.make max_x 0.0
  and p2 = Point.make max_x max_y
  and p3 = Point.make 0.0 max_y in
  let cond_point p = (
    (Point.equals p p0) ||
    (Point.equals p p1) ||
    (Point.equals p p2) ||
    (Point.equals p p3)
  ) 
  in
  
  let cond_triangle t = 
    let a, b, c = 
      Triangle.get_points t
    in
    (cond_point a) || (cond_point b) ||	(cond_point c)
  in
  
  Aset.filter tr_set (fun t -> not (cond_triangle t));;

(** Compute a grid of point space one step. Retourn this set of points.
    	- amesh : mesh.
 **)
let grid_points_from amesh =
  let x, y = 
    Point.float_coord_of (get_start_point amesh)
  and step = 
    get_step amesh
  and offset = 
    get_offset amesh
  in
  
  (** Fill a set of point with a grid of point.
		- a : float, x coordinate of the next point.
		- b : float, y coordinate of the next point.
		- pt_set : point aset, used as an accumulator.
   **)
  let rec create_point a b pt_set=
    if a > (x +. offset) then
      pt_set
    else
      begin
        if b >= (y +. offset) then
          create_point 
            (a +. step)
            y
            (Aset.insert pt_set (Point.make a b))
        else
          create_point
            a
            (b +. step)
            (Aset.insert pt_set (Point.make a b))
      end
  in 
  clean_point_set amesh (create_point x y (Aset.make Point.equals));;

(** Return a set of points containing points between 'pmin' and 'pmax'
    such as every point have the same threshold from the other.
		- amesh : mesh.
    	- threshold : float, maximum gap value on the z axis between two
    points.
    	- pmin : point.
    	- pmax : point.
 **)
let rec fill_inter_point_space amesh threshold pmin pmax =
  let f = 
    get_fun amesh 
  in
  
  let x0, y0 = 
    Point.float_coord_of pmin
  and x1, y1 = 
    Point.float_coord_of pmax 
  in
  
  if (abs_float ((f x0 y0) -. (f x1 y1))) > threshold then
    begin
      let pmid = 
        (Point.div_scal (Point.add pmin pmax) 2.0)
      in
      
      let a = 
        (fill_inter_point_space amesh threshold pmin pmid)
      and b = 
        (fill_inter_point_space amesh threshold pmid pmax) 
      in 
      (Aset.union a b)
    end
  else
    begin
      let empty_pt_set = 
        (Aset.make Point.equals)
      in
      let pt_set1 = 
        Aset.insert empty_pt_set pmin
      in
      (Aset.insert pt_set1 pmax);
    end;;

(** Add all point in horizontal, vertical and diagonal according to
    fill_inter_point_space for a point (x, y), to a set of points.
    	- amesh : mesh.
    	- pt_set : point aset, current point set.
    	- threshold : float, maximum gap value on the z axis between two
    points.
    	- x : float, x coordinate of the point to evalue.
    	- y : float, y coordinate of the point to evalue.
 **)
let add_fill_point_to_set amesh pt_set threshold x y =
  let step = 
    get_step amesh
  in

  let p = Point.make x y
  and p1 = Point.make (x -. step) y
  and p2 = Point.make x (y -. step) 
  and p3 = Point.make (x -. step) (y -. step) in

  let additional_points0 = 
    fill_inter_point_space amesh threshold p1 p
  and additional_points1 = 
    fill_inter_point_space amesh threshold p2 p
  and additional_points2 = 
    fill_inter_point_space amesh threshold p3 p
  in

  let tmp_set0 = 
    Aset.union additional_points0 additional_points1
  in
  let tmp_set1 = 
    Aset.union additional_points2 tmp_set0
  in 

  Aset.insert (Aset.union pt_set tmp_set1) (Point.make x y);;

(** Return a point set containing a grid of point whose density increase
    as the 3d function increase faster. The maximum gap value between 
    two z value is defined by the threshold.
    	- amesh : mesh.
    	- threshold : float, maximum gap value on the z axis between two
    points.
 **)
let smart_points_from amesh threshold =
  let x, y = 
    Point.float_coord_of (get_start_point amesh)
  and step = 
    get_step amesh
  and offset = 
    get_offset amesh in
  let rec create_point a b pt_set=
    if a > (x +. offset) then
      pt_set
    else
      begin
        let current_points_set = 
          add_fill_point_to_set amesh pt_set threshold a b
        in
        if b >= (y +. offset) then
          begin
            create_point (a +. step) y current_points_set;
          end
        else
          begin
            create_point a (b +. step) current_points_set;
          end
      end
  in
  clean_point_set amesh (create_point x y (Aset.make Point.equals));;

(* DECLIVITY COMPUTING FUNCTIONS*)

(* helper maths functions for triangle_declivity *)

(** Return the scalar product of two vectors. A vector is a triplet of
    float.
    	- u : (float * float * float).
    	- v : (float * float * float).
 **)
let dot u v =
  let ux,uy,uz = u
  and vx,vy,vz = v in
  
  ((ux *. vx) +. (uy *. vy) +. (uz *. vz));;

(** Return the vector product of two vectors.
		- u : (float * float * float).
    	- v : (float * float * float).
 **)
let vector_product u v= 
  let ux,uy,uz = u
  and vx,vy,vz = v in

  let nx = 
    ((uy *. vz) -. (uz *. vy))
  and ny = 
    ((uz *. vx) -. (ux *. vz))
  and nz = 
    ((ux *. vy) -. (uy *. vx)) in

  (nx,ny,nz);;

(** Compute the triangle angle with the vertical axe :
     0 is flat (tr is parrallel to P={(x,y,z),z=0})
     +-pi/2 is perpendicular (tr is perpendicular to P 
    	- amesh : mesh.
    	- tr : triangle.
 **)
let triangle_declivity amesh tr =
  let p0, p1, p2 = Triangle.get_points tr in

  let z0 = 
    get_z amesh p0
  and z1 = 
    get_z amesh p1
  and z2 = 
    get_z amesh p2
  in

  let ux, uy =
    Point.float_coord_of (Point.sub p1 p0) 
  and uz = 
    (z1 -. z2)
  and vx, vy =
    Point.float_coord_of (Point.sub p2 p0) 
  and vz = 
    z2 -. z0 
  in

  let u = (ux,uy,uz) 
  and v = (vx,vy,vz) in

  let n = 
    vector_product u v
  and z = 
    (0.0,0.0,1.0)
  in 

  let k = 
    vector_product z n
  in
  let sinalpha = 
    sqrt ((dot k k) /. (dot n n))
  in

  asin sinalpha;;

(* HEIGHT COMPUTING *)

(* In order to be more precise, we will use the centroid and not the
   circumscribed circle center of the triangle. *)
(** Return the height (z value) of the triangle centroid.
    	- amesh : mesh.
    	- tr : triangle.
 **)
let triangle_height amesh tr =
  let f = 
    get_fun amesh
  in
  let p0,p1,p2 = 
    Triangle.get_points tr
  in
  let pf = 
    Point.div_scal (Point.add p0 (Point.add p1 p2)) 3.0
  in
  let x,y =
    Point.float_coord_of pf
  in
  (f x y);;

(* EXTREMUM COMPUTING FUNCTION *)

(** Compute the couple of minimum and maximum z value for current mesh.
    	- amesh : mesh.
    	- tr_set : triangle aset, set representing the delaunay
    triangulation of the mesh.
 **)
let compute_extremum_height amesh tr_set =
  let h0 = 
    triangle_height amesh (Aset.get tr_set)
  in
  
  (** Auxiliary recursive function used to range a mesh according to
      a triangle set.
      	- tr_set0 : triangle set.
      	- minh : float, current minimum value found.
      	- maxh : float, current maximum value found.
   **)
  let rec aux tr_set0 (minh : float) (maxh : float)=
    if Aset.is_empty tr_set0
    then
      (minh, maxh)
    else
      begin
        let tr_set1, tr = 
          Aset.get_and_remove tr_set0 
        in
        
        let htr = 
          triangle_height amesh tr
        in
        
        let minh1 = (min minh htr)
        and maxh1 = (max maxh htr) in
        
        aux tr_set1 minh1 maxh1
      end
  in 
  
  aux tr_set h0 h0;;

(** Compute the couple of minimum and maximum angle value for
    current mesh.
    	- amesh : mesh.
    	- tr_set : triangle aset, set representing the delaunay
    triangulation of the mesh.
 **)
let compute_extremum_declivity amesh tr_set =
  let angle0 = 
    triangle_declivity amesh (Aset.get tr_set) 
  in
  
  let rec aux tr_set min_a max_a =
    if Aset.is_empty tr_set then (min_a, max_a)
    else
      begin
        let tr_set1, tr = 
          Aset.get_and_remove tr_set
        in
        let angle = 
          triangle_declivity amesh tr
        in
        let min_angle = 
          min min_a angle
        and max_angle = 
          max max_a angle 
        in
        aux tr_set1 min_angle max_angle
      end
  in
  aux tr_set angle0 angle0;;

(* COLORING FUNCTIONS *)

(** Define color based on triangle height.
    	- color0 : (float, float, float), base color.
    	- tr : triangle, triangle to color.
 **)
let triangle_color_height color0 amesh minh maxh tr =
  let htr = 
    triangle_height amesh tr
  in
  let percentage = 
    (1.0 -. ((htr -. minh) /. (maxh -. minh)))
  in
  let r, g, b = color0 in
  let r1 = 
    int_of_float (r *. (percentage))
  and g1 = 
    int_of_float (g *. (percentage))
  and b1 = 
    int_of_float (b *. (percentage))
  in
  (r1, g1, b1);;

(** Define color based on triangle declivity.
    	- color0 : (float, float, float), base color.
    	- tr : triangle, triangle to color.
 **)
let triangle_color_declivity color0 amesh min_a max_a tr =
  let angle = 
    triangle_declivity amesh tr 
  in
  let percentage = 
    let perc = 
      (1.0 -. ((angle -. min_a) /. (max_a -. min_a)))
    in
    if perc > 1.0 then 1.0
    else 
      if perc < 0.0 then 0.0
      else perc
  in
  let r, g, b = color0 in
  let r1 = 
    int_of_float (r *. (percentage))
  and g1 = 
    int_of_float (g *. (percentage))
  and b1 = 
    int_of_float (b *. (percentage))
  in
  (r1, g1, b1);;
