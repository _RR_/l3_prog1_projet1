#load "point.cmo";;
#load "aset.cmo";;

type vertice = {p0 : Point.point; p1 : Point.point};;

let make_vertice p0 p1=
  ({ p0 = p0; p1= p1} : vertice);; 

let get_p0 v =
  v.p0;;

let get_p1 v =
  v.p1;;

type 'a aarray = ('a array);;

let make_array n p = Array.make n p;;

let length_array ar = Array.length ar;;

let get_array ar i = ar.(i);;

let array_of_set pt_set =
  let n = Aset.length pt_set in
  let m = make_point_array n (Aset.get pt_set) in
  let rec aux pt_set0 i =
    if Aset.is_empty pt_set0
    then
      m
    else
      begin
        let pt_set1,pt = Aset.get_and_remove pt_set0
        in m.(i) <- pt;
        aux pt_set1 (i+1)
      end
  in aux pt_set 0;;

(* type list *)

let comp p0 p1 =
  if Point.equals p0 p1
  then
    0
  else
    begin
      if Point.is_inferior_to p0 p1
      then
        -1
      else
        1
    end;;

let sort_point_array pt_array =
  Array.sort comp pt_array;;
(* use Array.sort *)

