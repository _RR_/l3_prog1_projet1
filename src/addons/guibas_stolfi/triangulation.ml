#load "point.cmo"
      #load "matrix.cmo"
            #load "triangle.cmo"
                  #load "aset.cmo"

let triangulation pt_set =
  _   let pt_vec = array_of_set pt_set;;


let divide start finish =
  (debut, (fin+debut)/2, fin);;

let vertice_start vertice_array pt_array start finish =
  if (fin - debut) = 1
  then
    begin
      vertice_array.(debut) := (fin::!(vertice_array.(debut)));
      vertice_array.(fin) := (debut::!(vertice_array.(fin)));
    end 
      if (fin - debut) = 2
      then
        begin
          vertice_array.(debut) := (fin::(debut+1)::!(vertice_array.(debut)));
          vertice_array.(debut+1) := (fin::debut::!(vertice_array.(debut)));
          vertice_array.(fin) := ((debut+1)::debut::!(vertice_array.(debut)));
        end;;


let fusion start1 finish2 =
  let _,mid,_ = divide start1 finish2 in

;;


let vertice_equals v0 v1 =
  let p0, q0 = v0 and
  p1, q1 = v1 in
  ((Point.equals p0 p1) && (Point.equals q0 q1));;

let generate_vertices tr =
  let p0, p1, p2 = Triangle.get_points tr
  in
  let pmin = Point.min_point p0 (Point.min_point p1 p2)
  and pmax = Point.max_point p0 (Point.max_point p1 p2)
  and pmid = Point.min_point
      (Point.max_point p0 (Point.min_point p1 p2))
      (Point.max_point p1 p2)
  in
  ((pmin, pmid), (pmin, pmax), (pmid, pmax));;

let in_list a l =
  let rec in_list_aux l0 =
    match l0 with
    | [] -> false
    | t::q when (vertice_equals a t) -> true
    | _::q  -> in_list_aux q
  in in_list_aux l;;

(* compute convex border *)
let border triangle_set =
  let rec triangles_to_verticelist tr_set v_list=
    if Aset.is_empty tr_set
    then
      (v_list)
    else
      begin
        let (tr_set1,tr) = Aset.get_and_remove tr_set in
        let v0,v1,v2 = generate_vertices tr in
        triangles_to_verticelist tr_set1 (v0::v1::v2::v_list)
      end
  in
  let rec remove_multi_occurences lst =
    match lst with
    | [] -> []
    | a::q ->
      if in_list a q then
        begin
          let v_not_equal x = (not (vertice_equals x a)) in
          remove_multi_occurences (List.filter v_not_equal q)
        end
      else
        a::(remove_multi_occurences q)
  in
  remove_multi_occurences (triangles_to_verticelist triangle_set []);;

let triangle_to_insert triangle_set point =
  let create_triangle_from_vertice v =
    let p0, p1 = v in
    if (Triangle.is_counterclockwise p0 p1 point)
    then
      (Triangle.make p0 p1 point)
    else
      (Triangle.make p1 p0 point)
  in
  let rec create_triangle_from_vertices v_list acc =
    match v_list with
    | [] -> acc
    | v::q -> (create_triangle_from_vertices q
                 (Aset.insert acc
                    (create_triangle_from_vertice v)))
  in
  let vertices = border triangle_set in
  create_triangle_from_vertices vertices (Aset.make Triangle.equals);;

(* Add the two basics triangles *)
let generate_initial_config max_x max_y =
  let x = float_of_int max_x
  and y = float_of_int max_y
  and z = 0.0 in
  let p0 = Point.make z z
  and p1 = Point.make x z
  and p2 = Point.make z y
  and p3 = Point.make x y in
  let tr0 = Triangle.make p0 p1 p2
  and tr1 = Triangle.make p1 p3 p2 in
  let tr_set0 = Aset.make Triangle.equals in
  let tr_set1 = Aset.insert tr_set0 tr1 in
  let tr_set2 = Aset.insert tr_set1 tr0 in
  tr_set2;;

let add_point tr_set p =
  let filter tr = (Triangle.in_circle tr p) in
  let tr_to_remove = Aset.filter tr_set filter in
  let tr_set2 = (triangle_to_insert tr_to_remove p) in
  let tr_set3 = (Aset.diff tr_set tr_to_remove) in
  let tr_set4 = (Aset.union tr_set3 tr_set2) in
  tr_set4;;

let delaunay_step point_set tr_set =
  if Aset.is_empty point_set then
    (point_set, tr_set)
  else
    begin
      let point_set2, p = Aset.get_and_remove point_set in
      (point_set2, add_point tr_set p)
    end;;

(* compute delaunay triangulation *)
let delaunay points max_x max_y =
  let rec aux point_set tr_set=
    if Aset.is_empty point_set then
      tr_set
    else
      begin
        let point_set2,p = Aset.get_and_remove point_set in
        let f tr = (Triangle.in_circle tr p) in
        let tr_to_remove = Aset.filter tr_set f in
        let tr_set2 = (triangle_to_insert tr_to_remove p) in
        let tr_set3 = (Aset.diff tr_set tr_to_remove) in
        let tr_set4 = (Aset.union tr_set3 tr_set2) in
        aux point_set2 tr_set4
      end
  in
  aux points (generate_initial_config max_x max_y);;

let delaunay_divide pt_set =

  let rec divide pt_set (pt_set 
