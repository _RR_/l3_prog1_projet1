type matrix

val make : int -> float -> matrix
val set : matrix -> int -> int -> float -> unit
val get : matrix -> int -> int -> float
val sub_matrix : matrix -> int -> int -> matrix
val det : matrix -> float
val size : matrix -> int
val print : matrix -> unit
