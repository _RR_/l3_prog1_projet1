(** Define a matrix by a two dimensions float array **)
type matrix = float array array;;

(** Build a matrix of size 'size'x'size' initializated with the value
    'placeholder'.
    	- size : int, size of the square matrix.
    	- placeholder : float, default value of the matrix	
 **)
let make size placeholder =
  let mat = 
    Array.make_matrix size size placeholder
  in 
  mat;;

(** Set the value lambda in the matrix m. 
    	- m : matrix.
    	- i : int, line rank of the matrix.
    	- j : int, column rank of the matrix.
    	- lambda : float, value to set in the matrix.
 **)
let set m i j lambda =
  m.(i).(j) <- lambda;;

(** Return the value of rank i j in the matrix, float.
    	- m : matrix.
    	- i : int, line rank of the matrix.
    	- j : int, column rank of the matrix.
 **)
let get m i j =
  m.(i).(j);;

(** Return the matrix size, float.
    	- m : matrix.
 **)
let size m =
  Array.length m;;

(** Compute and return the submatrix of matrix 'm'. The submatrix is
    the matrix 'm' without the line 'i' and the column 'j'.
    	- m : matrix, matrix to subdivide.
    	- i : int, line rank which will be removed.
    	- j : int, column rank which will be removed.
 **)
let sub_matrix m i j =
  let n = 
    Array.length m
  in
  let mat2 = 
    make (n-1) (0.0)
  in
  (* duplicate upper left *)
  for k1=0 to (i-1) do
    for k2=0 to (j-1) do
      mat2.(k1).(k2) <- m.(k1).(k2);
    done;
  done;
  (* lower left *)
  for k1=(i+1) to (n-1) do
    for k2=0 to (j-1) do
      mat2.(k1-1).(k2) <- m.(k1).(k2);
    done;
  done;
  (* upper right *)
  for k1=0 to (i-1) do
    for k2=(j+1) to (n-1) do
      mat2.(k1).(k2-1) <- m.(k1).(k2);
    done;
  done;
  (* lower right *)
  for k1=(i+1) to (n-1) do
    for k2=(j+1) to (n-1) do
      mat2.(k1-1).(k2-1) <- m.(k1).(k2);
    done;
  done;
  mat2;;

(** Return the determinant of the matrix, float.
    	- m : matrix.
 **)
let rec det (m :float array array)=
  let n = 
    Array.length m
  in
  if n = 1 then 
    (Counter.inc_det();  m.(0).(0))
  else
  if n = 2 then
    (
      Counter.inc_det();
      ((m.(0).(0)*.m.(1).(1)) -. (m.(0).(1)*.m.(1).(0)))
    )
  else
    begin
      let d = ref 0.0 
      and s = ref 1.0 in
      for i=0 to (n-1) do
        d := ((!d) +. ((!s) *. (m.(i).(0)*.det (sub_matrix m i 0))));
        s := !s*.(-1.0);
      done;
      !d;
    end;;

(** Print the matrix in the console. 
    	- m : matrix.
 **)
let print m =
  print_newline ();
  let n = Array.length m in
  for i=0 to (n-1) do
    for j=0 to (n-1) do
      print_float m.(i).(j);
      print_string "\t";
    done;
    print_newline ();
  done;;
