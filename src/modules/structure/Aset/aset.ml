(** Define an set by a list and a comparaison function. **)
type 'a aset = ( ('a list) * ('a -> 'a -> bool));;

(** Build a set of type 'a with a comparison function.
    	- compare_function : ('a -> 'a -> bool), function using to test
    the equality of two objects of type 'a.
 **)
let make compare_function =
  (([], compare_function): 'a aset)
;;

(** Return the length of the set, int.
    		- s : 'a aset.
 **)
let length s =
  List.length (fst s)
;;

(** Return true if the set is empty, else false.
    		- s : 'a aset.
 **)
let is_empty (s: 'a aset) =
  length s == 0
;;

(** Return the first element of the set, 'a.
    This is an internal function
    		- set : 'a aset.
 **)
let get_hd set =
  (List.hd (fst set))
;;

(** Return the element of the set without the first one, 'a aset.
    This is an internal function
    		- set : 'a aset.
 **)
let get_tl set =
  (List.tl (fst set))


(** Return true if an element is equal to an element of the same type
    'thing', else false.
    		- set : 'a aset.
    		- thing : 'a, element search in the set.
 **)
let exists set thing =
  List.exists 
    (function x -> (snd set) x thing)
    (fst set)
;;

(** Add an element of type 'a to the set 'set'.
    		- set : 'a aset.
    		- thing : 'a, value to add to the set.
 **)
let insert set thing =
  if not (exists set thing) then
    ((List.cons thing (fst set)), (snd set))
  else
    set
;;

(** Add 'n' random elements of type 'a to the set.
    		- set : 'a set.
    		- n : int, number of element to add to the set.
    		- make_fun : (unit -> 'a), function used to create random
    object of type 'a.
 **)
let rec fill_with_random set n make_fun =
  if n == 0 then set
  else
    insert 
      (fill_with_random set (n-1) make_fun)
      (make_fun())
;;

(** Remove the value 'thing' from the set.
    		- set : 'a aset.
    		- thing : 'a, object to remove from the set.
 **)
let rec remove (set: 'a aset) (thing:'a) =
  if (is_empty set) then
    set
  else
    (
      if ((snd set) (get_hd set) thing) then
        (get_tl set, (snd set))
      else
        (
          (List.cons
             (get_hd set)
             (fst (remove ((get_tl set), (snd set)) thing))),
          (snd set)
        )
    )
;;

(** Return the first element of the set.
    		- set : 'a aset.
 **)
let get (set: 'a aset) =
  if is_empty set then
    failwith "get"
  else
    get_hd set
;;

(** Return the first element of the set, and remove it from the set,
    this function will return a couple composed of the first element 
    of the set, and of the set's tail.
    	- set : 'a aset.
 **)
let get_and_remove set =
  if is_empty set then
    failwith "get_rm"
  else
    let el = get_hd set in
    ((remove set el), el)
;;

(** Apply a function to each element of the set, returning a new set
    composed of each result.
    	- set : 'a aset.
    	- f : ('a -> 'b), function to apply to each elements of the set.
    	- new_comparator : ('b -> 'b -> bool), function testing the
    equality of two elements of type 'b. Used to make the new 'b aset.
 **)
let map (set:'a aset) (f:'a -> 'b) (new_comparator: 'b -> 'b -> bool) =
  ((List.map f (fst set)), new_comparator)
;;

(** Apply a function with bord effect to each element of the set.
    		- set : 'a aset.
    		- f : ('a -> unit), function to apply to each elements of
    the set.
 **)
let iter (set: 'a aset) (f:('a -> unit)) =
  (List.iter f (fst set))
;;

(** Apply a function to each element of the set, returning a new set
    composed of each element of 'set' passing the filter function.
    	- set : 'a aset.
    	- f : ('a -> bool), function used to filter the element the set.
 **)
let filter (set:'a aset) (f:'a -> bool) =
  ((List.filter f (fst set)), (snd set))
;;

(** Print all the element of a set according to a print function.
    	- set : 'a aset.
    	- print_fun : ('a -> unit ()), function printing an element of
    type 'a in the console.
 **)
let rec print set print_fun =
  if is_empty set then
    print_newline()
  else
    (
      print_fun (get_hd set); print_string " ";
      print ((get_tl set), (snd set)) print_fun
    )
;;

(** Return the intersection of two sets.
    		- set1 : 'a aset.
    		- set2 : 'a aset.
 **)
let inter (set1:'a aset) (set2:'a aset) =
  let is_in_set2 = 
    fun t -> exists set2 t
  in
  ((fst (filter set1 is_in_set2)), (snd set1))
;;

(** Return the union of two sets.
    		- set1 : 'a aset.
    		- set2 : 'a aset.
 **)
let union set1 set2 =
  let is_not_in_set1 = 
    fun t -> not (exists set1 t)
  in
  (List.append (fst set1)
     (fst (filter set2 is_not_in_set1)), (snd set1))
;;

(** Return the difference of two sets.
    		- set1 : 'a aset.
    		- set2 : 'a aset.
 **)
let diff (set_to_keep:'a aset) (set_to_remove: 'a aset) =
  let inter_set =
    inter set_to_keep set_to_remove
  in
  let is_not_in_set_inter =
    fun t -> not (exists inter_set t)
  in
  (fst (filter set_to_keep is_not_in_set_inter), snd set_to_keep)
;;
