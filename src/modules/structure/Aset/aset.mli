type 'a aset

val make : ('a -> 'a -> bool) -> 'a aset
val is_empty : 'a aset -> bool
val length : 'a aset -> int
val insert : 'a aset -> 'a -> 'a aset
val fill_with_random : 'a aset -> int -> (unit -> 'a) -> 'a aset
val get : 'a aset -> 'a
val get_and_remove : 'a aset -> 'a aset * 'a
val remove : 'a aset -> 'a -> 'a aset
val exists : 'a aset -> 'a -> bool
val map : 'a aset -> ('a -> 'b) -> ('b -> 'b -> bool) -> 'b aset
val filter : 'a aset -> ('a -> bool) -> 'a aset
val iter : 'a aset -> ('a -> unit) -> unit
val inter :  'a aset -> 'a aset -> 'a aset
val union :  'a aset -> 'a aset -> 'a aset
val diff :  'a aset -> 'a aset -> 'a aset

val print : 'a aset -> ('a -> unit) -> unit
