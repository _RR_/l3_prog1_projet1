type triangle

val make : Point.point -> Point.point -> Point.point -> triangle
val get_points : triangle -> (Point.point * Point.point * Point.point)
val equals : triangle -> triangle -> bool
val in_circle : triangle -> Point.point -> bool
val is_counterclockwise : 
	Point.point -> Point.point -> Point.point -> bool
val print : triangle -> unit
