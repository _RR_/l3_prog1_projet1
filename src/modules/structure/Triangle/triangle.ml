(** Define a triangle by three points. **)
type triangle =	{
  pointA : Point.point;
  pointB : Point.point;
  pointC : Point.point
};;

(** Return a triplet of the three triangle's points, 
    (point * point * point).
    	- t : triangle.
 **)
let get_points t = 
  (t.pointA, t.pointB, t.pointC);;

(** Return true if the three points 'p1', 'p2' and 'p3' is
    counterclockwise, else false.
    	- p1 : point.
    	- p2 : point.
    	- p3 : point.
 **)
let is_counterclockwise p1 p2 p3 =
  let matrix = 
    Matrix.make 2 (1.0) 
  in

  let ax, ay = 
    Point.float_coord_of p1
  and bx, by =
    Point.float_coord_of p2
  and cx, cy =
    Point.float_coord_of p3
  in

  Matrix.set matrix 0 0 (bx -. ax);
  Matrix.set matrix 0 1 (cx -. ax);
  Matrix.set matrix 1 0 (by -. ay);
  Matrix.set matrix 1 1 (cy -. ay);

  let det_matrix = 
    Matrix.det matrix 
  in
  (det_matrix >= 0.0);;

(** Return the matrix used to test if a point is inside the
    circumscribed circle of a triangle.
    	- t : triangle.
    	- p : point, point to test.
 **)
let matrix_of_triangle_and_point t d =
  let matrix =
    Matrix.make 3 (1.0)
  in

  let a, b, c = 
    get_points t 
  in

  let a_sub_d = Point.sub a d
  and b_sub_d = Point.sub b d
  and c_sub_d = Point.sub c d in

  let v = 
    [| a_sub_d; b_sub_d; c_sub_d|]
  in

  for i = 0 to 2 do
    let ax, ay = 
      Point.float_coord_of (v.(i))
    in
    Matrix.set matrix i 0 ax;
    Matrix.set matrix i 1 ay;
    Matrix.set matrix i 2 ((ax *. ax) +. (ay *. ay));
  done;
  matrix;;

(** Build a triangle whose vertex is 'p1', 'p2' and 'p3'.
    	- p1 : point.
    	- p2 : point.
    	- p3 : point.
 **)
let make p1 p2 p3 = 
  {pointA = p1; pointB = p2; pointC = p3};;

(** Return true if two triangles are equals, else false.
    	- t1 : triangle.
    	- t2 : triangle.
 **)
let equals t1 t2 =
  let contains_point t p =
    ((Point.equals t.pointA p)
     || (Point.equals t.pointB p)
     || (Point.equals t.pointC p))
  in
  contains_point t2 t1.pointA &&
  contains_point t2 t1.pointB &&
  contains_point t2 t1.pointC;;

(** Return true if the point 'p' is inside the circumscribed circle of
    the triangle 't'.
    	- t : triangle.
    	- p : point.
 **)
let in_circle t p =
  Counter.inc_in_circle();
  let matrix = 
    matrix_of_triangle_and_point t p
  in
  let det_matrix =
    Matrix.det matrix
  and order =
    is_counterclockwise t.pointA t.pointB t.pointC
  in
  (((det_matrix > 0.0) && order) ||
   ((det_matrix < 0.0) && (not order)));;

(** Print a triangle in the console.
    	- t : triangle.
 **)
let print t =
  let print_point p =
    let x, y = 
      Point.float_coord_of p 
    in
    "("^(string_of_float x)^", "^(string_of_float y)^")";
  in
  print_string ("( A : " ^ (print_point t.pointA) ^ "; ");
  print_string ("B : " ^ (print_point t.pointB) ^ "; ");
  print_string ("C : " ^ (print_point t.pointA) ^ ")");
  print_newline();;
