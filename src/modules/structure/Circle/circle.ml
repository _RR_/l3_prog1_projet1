(** Define a circle by its center and its radius. **)
type circle = {center : Point.point; radius : float}

(** Build a circle.
		- center : point, point defining the circle center.
    	- radius : float, radius of the circle.
 **)
let make center radius = {center = center; radius = radius};;

(** Return the circle's center, float.
    	- c : circle.
 **)
let coord_of_center c = c.center;;

(** Return the circle's radius, float.
    	- c : circle.
 **)
let get_radius c =  c.radius;;

(** Return true if the circle c1 equals the circle c2, else false.
    	- c1 : circle.
    	- c2 : circle, circle to compare with c1.
 **)
let equals c1 c2 =  
  (Point.equals c1.center c2.center)
  && (c1.radius = c2.radius);;

(** Compute the circumscribed circle of a triangle. Return a circle.
    	- t : triangle.
 **)
let circle_from_triangle t =
  let a, b, c = 
    Triangle.get_points t
  in

  let ax, ay = 
    Point.float_coord_of a
  and bx, by = 
    Point.float_coord_of b
  and cx, cy = 
    Point.float_coord_of c in

  let norm_a = (ax *. ax +. ay *. ay)
  and norm_b = (bx *. bx +. by *. by)
  and norm_c = (cx *. cx +. cy *. cy) in

  let d = 
    2.0 *. (ax *. (by -. cy) +. bx *. (cy -. ay) +. cx *. (ay -. by)) 
  in

  let ux = 
    (1.0 /. d) *. (norm_a *. (by -. cy) 
                   +. norm_b *. (cy -. ay) +. norm_c *. (ay -. by))
  and uy = 
    (1.0 /. d) *. (norm_a *. (cx -. bx) 
                   +. norm_b *. (ax -. cx) +. norm_c *. (bx -. ax))
  in

  let center = 
    Point.make ux uy 
  in
  let radius = 
    sqrt ((ax -. ux)**(2.0) +. (ay -. uy)**(2.0))
  in
  make center radius;;
