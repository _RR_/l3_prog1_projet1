type circle

val make : Point.point -> float -> circle
val coord_of_center : circle -> Point.point
val get_radius : circle -> float
val equals : circle -> circle -> bool
val circle_from_triangle : Triangle.triangle -> circle
