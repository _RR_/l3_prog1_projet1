type point

val make : float -> float -> point
val int_coord_of : point -> (int * int)
val float_coord_of : point -> (float * float)
val invert : point -> point
val add : point -> point -> point
val sub : point -> point -> point
val times_scal : point -> float -> point
val div_scal : point -> float -> point
val equals : point -> point -> bool
val max_point : point -> point -> point
val min_point : point -> point -> point
val random : int -> int -> point
val norm : point -> float
val print : point -> unit
val is_inferior_to : point -> point -> bool
