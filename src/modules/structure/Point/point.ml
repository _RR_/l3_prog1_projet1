(** Define a point by it x and y coordinates. **)
type point = {x : float; y : float}

(** Build a point of coordinate 'x', 'y'.
    	- x : float.
    	- y : float.
 **)
let make x y =
  {x = x; y = y};;

(** Return the coordinate of point 'p', (int * int).
    	- p : point.
 **)
let int_coord_of p =
  ((int_of_float p.x),  (int_of_float p.y));;

(** Return the coordinate of point 'p', (float * float).
    	- p : point.
 **)
let float_coord_of p =  (p.x,  p.y);;

(** Return a point which is the invert of point 'p'.
    	- p : point.
 **)
let invert p =
  make (0.0 -. p.x) (0.0 -. p.y);;

(** Return a point which is the addition of two points.
    	- p1 : point.
    	- p2 : point.
 **)
let add p1 p2 =
  make (p1.x +. p2.x) (p1.y +. p2.y);;

(** Return a point which is the substraction of two points.
    	- p1 : point.
    	- p2 : point.
 **)
let sub p1 p2 =
  make (p1.x -. p2.x) (p1.y -. p2.y);;

(** Return a point whose coordinates are the coordinates of point 'p'
    times a float number.
    	- p : point.
    	- k : float.
 **)
let times_scal p k = 
  make (p.x *. k) (p.y *. k);;

(** Return a point whose coordinates are the coordinates of point 'p'
    divide by a float number.
    	- p : point.
    	- k : float.
 **)
let div_scal p k = 
  make (p.x /. k) (p.y /. k);;

(** Return true if the two points have the same coordinates, else false.
    	- p1 : point.
    	- p2 : point.
 **)
let equals p1 p2 = 
  (p1.x = p2.x) && (p1.y = p2.y);;

(** Use to define a lexicographic order on point, return true if 'p0' is
    inferior to 'p1', else false.
    	- p1 : point.
    	- p2 : point.
 **)
let is_inferior_to p0 p1 =
  ((p0.x < p1.x) || 
   ((p0.x = p1.x) && (p0.y < p1.y)));;

(** Return the bigger point between 'o0' and 'p1' according to the
    order, point.
    	- p0 : point.
    	- p1 : point.
 **)
let max_point p0 p1 =
  if is_inferior_to p0 p1 then
    p1
  else
    p0
;;

(** Return the smaller point between 'o0' and 'p1' according to the
    order, point.
    	- p0 : point.
    	- p1 : point.
 **)
let min_point p0 p1 =
  if is_inferior_to p0 p1 then
    p0
  else
    p1
;;

(** Return the distance between the origin (0.0, 0.0) and the point,
    float.
    	- p : point.
 **)
let norm p =
  sqrt ((p.x *. p.x) +. (p.y *. p.y));;

(** Generate a random point whose coordinates are between 1 and 
    'max_x' - 1, and between 1 and 'max_y' - 1, point.
    	- max_x : int, maximum value for the x coordinate.
    	- max_y : int, maximum value for the y coordinate.
 **)
let random max_x max_y =
  Random.self_init();
  make
    (float_of_int ((Random.int (max_x - 2)) + 1))
    (float_of_int ((Random.int (max_y - 2)) + 1))
;;

(** Print in the console the point 'p'.
    	- p : point.
 **)
let print p =
  print_string "(";
  print_float p.x;
  print_string ", ";
  print_float p.y;
  print_string ")";
  print_newline()
;;
