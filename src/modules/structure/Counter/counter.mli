val inc_det : unit -> unit
val value_det : string -> unit
val init_det : unit -> unit

val inc_in_circle : unit -> unit
val value_in_circle : string -> unit
val init_in_circle : unit -> unit
