#directory "../../../build";;

#use "../../../triangulation.ml";;

let gen_random_points n =
  Aset.fill_with_random 
    (Aset.make Point.equals)
    n
    (fun () -> Point.random 800 600);;

let list_nb = 
  [1;10;30;50;100;150;200;250;350;500;650;
  800;1000;1250;1500;1750;2000;2500;3000];;

let function_to_apply nb =
  let points = 
    gen_random_points nb 
  in
  
  Counter.init_det();
  Counter.init_in_circle();
  
  let triangles = 
    delaunay points 800 600 
  in
  
  let nb_str = 
    string_of_int nb 
  in
  
  Counter.value_det nb_str;
  Counter.value_in_circle nb_str;
  
  print_newline();;

List.iter function_to_apply list_nb;;
