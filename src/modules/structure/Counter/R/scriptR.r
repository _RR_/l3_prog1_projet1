data_det <- read.table("det.data", sep = ",", col.names=c("nb_points", "calls to det"))
pdf("det.pdf")
plot(data_det, type="o", col="blue")
title(main = "Number of calls to determinant for x points")

data_in_circle <- read.table("in_circle.data", sep = ",", col.names=c("nb_points", "calls to in_circle"))
pdf("in_circle.pdf")
plot(data_in_circle, type="o", col="blue")
title(main = "Number of calls to in_circle for x points")
###

#data_det <- read.table("det.data", sep = ",", col.names=c("nb_points", "calls to in_circle"))
##data_in_circle <- read.table("in_circle.data", sep = ",", col.names=c("nb_points", "calls to in_circle*13"))
#data_in_circle <- read.table("in_circle13_2.data", sep = ",", col.names=c("nb_points", "calls to in_circle*13"))
#pdf("compare.pdf")
#plot(data_in_circle, type="o", col="red", ann=FALSE, ylim=c(0, max(data_det, data_in_circle)))
##par(new=TRUE)
#plot(data_det, type="o", col="blue", ann=FALSE, ylim=c(0, max(data_det, data_in_circle)))
#title(main = "Number of calls for x points")
#title(xlab= "points", col.lab=rgb(0,0.5,0), font.lab=4)
#title(ylab= "calls", col.lab=rgb(0,0.5,0), font.lab=4)
###




q()
