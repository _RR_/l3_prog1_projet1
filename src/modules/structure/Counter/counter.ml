(** Define a counter used to count the number of determinant function
    call. **)
let counter_of_det = ref 0;;
(** Define a counter used to count the number of in_circle test call. 
 **)
let counter_of_in_circle = ref 0;;

(** Increased the determinant counter. **)
let inc_det () = counter_of_det  := !counter_of_det + 1;;

(** Increased the in_circle counter. **)
let inc_in_circle () = 
  counter_of_in_circle  := !counter_of_in_circle + 1;;

(** Return the value of the determinant counter for a task.
		- str : string, task name.
 **)
let value_det str = 
  print_string ("counter of DETERMINANT for " ^ str ^ " : "); 
  print_int (!counter_of_det); print_newline();;

(** Return the value of the in_circle counter for a task.
    	- str : string, task name.
 **)
let value_in_circle str = 
  print_string ("counter of IN_CIRCLE for " ^ str ^ " : ");
  print_int (!counter_of_in_circle); print_newline();;

(** Reinitialisate the determinant counter. **)
let init_det () =  counter_of_det := 0;;

(** Reinitialisate the in_circle counter. **)
let init_in_circle () =  counter_of_in_circle := 0;;
