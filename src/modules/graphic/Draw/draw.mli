val draw_point : Graphics.color -> Point.point -> unit
val draw_points : Graphics.color -> (Point.point) Aset.aset -> unit

val draw_line : Graphics.color -> Point.point -> Point.point -> unit

val draw_triangle : Graphics.color -> Triangle.triangle -> unit
val draw_triangles : 
	Graphics.color -> (Triangle.triangle) Aset.aset -> unit
	
val fill_triangle :
	(Triangle.triangle -> (int * int * int)) 
		-> Triangle.triangle -> unit
val fill_triangles :
	(Triangle.triangle -> (int * int * int)) 
		-> (Triangle.triangle) Aset.aset -> unit
	
val draw_c : Graphics.color -> Circle.circle -> unit
val draw_circles : 
	Graphics.color -> (Circle.circle) Aset.aset -> unit

val gradient_x :
	(float * float * float) -> (float * float * float) 
		-> float -> Triangle.triangle -> (int * int * int)

val gradient_y :
	(float * float * float) -> (float * float * float) 
		-> float -> Triangle.triangle -> (int * int * int)
		
val gradient_circle :
	(float * float * float) -> (float * float * float) 
		-> float -> Point.point -> Triangle.triangle 
			-> (int * int * int)
