#!/bin/sh
set -x
MODULE=draw

ocamlc -c -I ../../structure/Aset/ -I ../../structure/Counter/ -I ../../structure/Point/ -I ../../structure/Matrix/ -I ../../structure/Triangle/ -I ../../structure/Circle/ -o $MODULE.cmi $MODULE.mli
ocamlc -c -I ../../structure/Aset/ -I ../../structure/Counter/ -I ../../structure/Point/ -I ../../structure/Matrix/ -I ../../structure/Triangle/ -I ../../structure/Circle/ -o $MODULE.cmo $MODULE.ml
#ocamlc -a -o $MODULE.cma $MODULE.cmo

