open Graphics;;

(* Drawing functions *)
(** Draw a point of color 'color' in the graph.
    	- color : color, color of the point.
    	- point : point, point to draw.
 **)
let draw_point color p =
  set_color color;
  let x, y = 
    Point.int_coord_of p
  in
  fill_circle x y 3;;

(** Draw a set of points of color 'color' in the graph.
    	- color : color, color of the point.
    	- points : point aset, points to draw.
 **)
let draw_points color points =
  Aset.iter points (fun p -> draw_point color p);;

(** Draw a line of color 'color' in the graph.
    	- color : color, color of the point.
    	- origin : point, point defining the origin of the line.
    	- dest : point, point defining the end of the line.
 **)
let draw_line color origin dest =
  set_color color;
  set_line_width 2;
  let origin_x, origin_y = 
    Point.int_coord_of origin
  and dest_x, dest_y =
    Point.int_coord_of dest
  in
  moveto origin_x origin_y;
  lineto dest_x dest_y;;

(** Draw a triangle of color 'color' in the graph.
    	- color : color, color of the point.
    	- t : triangle, triangle to draw.
 **)
let draw_triangle color t =
  let p1, p2, p3 = 
    Triangle.get_points t
  in
  draw_line color p1 p2;
  draw_line color p3 p2;
  draw_line color p1 p3;;

(** Draw a set of triangles of color 'color' in the graph.
    	- color : color, color of the triangle.
    	- triangles : triangle aset, triangles to draw.
 **)
let draw_triangles color triangles =
  Aset.iter triangles (fun t -> draw_triangle color t);;

(** Draw a fill triangle in the graph, whose color is compute according
    a function.
    	- fill_function : (triangle -> (int * int * int)), function
    assigning a color to a triangle.
    	- t : triangle, triangle to draw.
 **)
let fill_triangle fill_function t = 
  let p1, p2, p3 =
    Triangle.get_points t 
  and r, g ,b  =
    (fill_function t)
  in
  set_color (rgb r g b);
  fill_poly 
    [| Point.int_coord_of p1;
    Point.int_coord_of p2;
    Point.int_coord_of p3 |];;

(** Draw a set of fill triangle in the graph, whose color is 
    compute according a function.
    	- fill_function : (triangle -> (int * int * int)), function
    assigning a color to a triangle.
    	- triangles : triangle aset, triangles to draw.
 **)
let fill_triangles fill_function triangles =
  Aset.iter triangles (fun t -> fill_triangle fill_function t);;

(** Draw a circle of color 'color' in the graph.
    	- color : color, color of the circle.
    	- c : circle, circle to draw.
 **)
let draw_c color c =
  set_color color;
  set_line_width 2;
  let p = 
    Circle.coord_of_center c
  and radius = 
    Circle.get_radius c 
  in
  let x, y = 
    Point.int_coord_of p
  in
  draw_circle x y (int_of_float radius);;

(** Draw a set of circles of color 'color' in the graph.
    	- color : color, color of the point.
    	- circles : circle aset, circles to draw.
 **)
let draw_circles color circles =
  Aset.iter circles (fun c -> draw_c color c);;

(** Return a value 'c', it is included between 0 and 1. Else, if it is
    less than 0, return 0; and if it is more than 1, return 1.
    	- c : float, value to frame.
 **)
let coefficient c = 
  if (c >= 0.0 && c <= 1.0) then c
  else
    if (c < 0.0) then 0.0
    else 1.0;;

(* Gradient function *)
(** Color defining function creating a color gradient on the x axis from
    a color to an other one.
    	- rgb_from : (float * float * float), starting color formatting
    in rgb.
    	- rgb_to : (float * float * float), final color formatting
    in rgb.
    	- max_x : float, maximum value on the x axis.
    	- triangle : triangle, triangle use to defind the color.
 **)
let gradient_x rgb_from rgb_to max_x triangle =
  let r0, g0, b0 = rgb_from
  and r1, g1, b1 = rgb_to in

  let circle = 
    Circle.circle_from_triangle triangle
  in
  let point = 
    Circle.coord_of_center circle
  in
  let x, _ = 
    Point.float_coord_of point
  in

  let r2 = 
    (r0 +. (r1 -. r0) *. (coefficient (x /. max_x))) 
  and g2 = 
    (g0 +. (g1 -. g0) *. (coefficient (x /. max_x))) 
  and b2 = 
    (b0 +. (b1 -. b0) *. (coefficient (x /. max_x)))
  in

  ((int_of_float r2), (int_of_float b2), (int_of_float g2));;

(** Color defining function creating a color gradient on the y axis from
    a color to an other one.
    	- rgb_from : (float * float * float), starting color formatting
    in rgb.
    	- rgb_to : (float * float * float), final color formatting
    in rgb.
    	- max_y : float, maximum value on the y axis.
    	- triangle : triangle, triangle use to defind the color.
 **)
let gradient_y rgb_from rgb_to max_y triangle =
  let r0, g0, b0 = rgb_from
  and r1, g1, b1 = rgb_to in

  let circle = 
    Circle.circle_from_triangle triangle 
  in
  let point = 
    Circle.coord_of_center circle
  in
  let _, y =
    Point.float_coord_of point
  in

  let r2 = 
    (r0 +. (r1 -. r0) *. (coefficient (y /. max_y))) 
  and g2 = 
    (g0 +. (g1 -. g0) *. (coefficient (y /. max_y))) 
  and b2 = 
    (b0 +. (b1 -. b0) *. (coefficient (y /. max_y)))
  in

  ((int_of_float r2), (int_of_float b2), (int_of_float g2));;

(** Color defining function creating a circulat color gradient centering
    on the point 'center' from a color to an other one.
    	- rgb_from : (float * float * float), starting color formatting
    in rgb.
    	- rgb_to : (float * float * float), final color formatting
    in rgb.
    	- radius : float, radius of the circular gradient.
    	- center : point, point defining the center of the circular
    gradient.
    	- triangle : triangle, triangle use to defind the color.
 **)
let gradient_circle rgb_from rgb_to radius center triangle = 
  let r0, g0, b0 = rgb_from
  and r1, g1, b1 = rgb_to in

  let circle =
    Circle.circle_from_triangle triangle 
  in
  let point = 
    Circle.coord_of_center circle 
  in
  let distance = 
    Point.norm (Point.sub center point) 
  in

  let r2 = 
    (r0 +. (r1 -. r0) *. (coefficient (distance /. radius))) 
  and g2 = 
    (g0 +. (g1 -. g0) *. (coefficient (distance /. radius))) 
  and b2 = 
    (b0 +. (b1 -. b0) *. (coefficient (distance /. radius))) 
  in

  ((int_of_float r2), (int_of_float b2), (int_of_float g2));;
