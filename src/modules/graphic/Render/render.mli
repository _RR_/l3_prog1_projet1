val create_window : int -> int -> unit
val init_screen : int -> int -> unit
val close_screen : unit -> unit

type environnement
val draw_env : environnement -> unit
val init_environnement :
	(((Triangle.triangle) Aset.aset) -> Point.point
		-> ((Triangle.triangle) Aset.aset))
			-> (((Point.point) Aset.aset) -> int 
				-> int -> ((Triangle.triangle) Aset.aset)) 
					-> int -> int -> int -> environnement
val skel : 
	(unit -> unit) ->  (unit -> unit) -> (char -> unit) 
		-> (int -> int -> unit) -> unit
		
val handle_key : 
	(environnement -> unit) -> environnement -> char -> unit
val handle_mouse : 
	(environnement -> unit) -> environnement -> int -> int -> unit
