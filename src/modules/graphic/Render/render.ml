open Graphics;;
open Draw;;

(* Windows creation functions *)
(** Open a window of size 'screen_width' x 'screen_height'.
    	- screen_width : int, width of the window.
    	- screen_height : int, height of the window.
 **)
let create_window screen_width screen_height=
  let param=
    " "^(string_of_int screen_width)
    ^"x"^(string_of_int screen_height)
    ^"-0+0" in open_graph param;
  set_window_title "Delaunay triangulation user interface";;

(** Initialize a window using double buffering.
    	- screen_width : int, width of the window.
    	- screen_height : int, height of the window.
 **)
let init_screen screen_width screen_height = 
  create_window screen_width screen_height;
  auto_synchronize false;;

(** Close the window. **)
let close_screen () = 
  close_graph ();;

(* Windows environnement variable *)
(** Define an environnement, it contains all the needed data to
    run the interface. **)
type environnement = {
  window_width : int;
  window_height : int;
  add_point_function :
    ((Triangle.triangle) Aset.aset) -> Point.point
    -> ((Triangle.triangle) Aset.aset);
  triangulation_function : 
    ((Point.point) Aset.aset) -> int -> int 
    -> ((Triangle.triangle) Aset.aset);
  mutable nb_point : int;
  mutable point_set : ((Point.point) Aset.aset);
  mutable triangle_set : ((Triangle.triangle) Aset.aset);
  mutable show_menu : bool;
  mutable show_point : bool;
  mutable show_triangle : bool;
  mutable show_circle : bool;
  mutable show_filltriangle : bool;
  mutable gradient_type : int;
  mutable colors : 
    ((float * float * float) * (float * float * float));
  mutable click : Point.point
};;

(* Environnement drawing*)
(** Return the color computing function according to the environnement
    gradient type.
    	- env : environnement.
 **)
let fill_color_function env = 		
  let rgb_from, rgb_to = 
    env.colors 
  in

  let fmax_x = 
    (float_of_int env.window_width)
  and fmax_y = 
    (float_of_int env.window_height)
  in

  match env.gradient_type with
  |0 -> (fun p -> gradient_x rgb_from rgb_to fmax_x p);
  |1 -> (fun p -> gradient_y rgb_from rgb_to fmax_y p);
  |2 -> (
      let rad = 
        sqrt(fmax_x**(2.0) +. fmax_y**(2.0)) /. 2.0 
      in
      (fun p -> gradient_circle rgb_from rgb_to rad env.click p)
    );
  |_ -> failwith "fill_color_function";;

(** Draw commands menu of the interface.
    		- env : environnement.
 **)
let draw_commands env = 
  set_color black;
  let width = 
    env.window_width
  and height = 
    env.window_height in

  moveto (width / 10) (height * 9 / 10);
  draw_string "User commands of the interface :";
  moveto (width * 2 / 10) (height * 9 / 10 - 20);
  draw_string "'c' : close the window";
  moveto (width * 2 / 10) (height * 9 / 10 - 40);
  draw_string "'a' : draw points";
  moveto (width * 2 / 10) (height * 9 / 10 - 60);
  draw_string "'z' : draw triangles";
  moveto (width * 2 / 10) (height * 9 / 10 - 80);
  draw_string "'e' : draw circumscribed circle";
  moveto (width * 2 / 10) (height * 9 / 10 - 100);
  draw_string "'r' : randomise triangulation with new points";
  moveto (width * 2 / 10) (height * 9 / 10 - 120);
  draw_string "'q' : draw fill triangle";
  moveto (width * 2 / 10) (height * 9 / 10 - 140);
  draw_string "'s' : randomise colors";
  moveto (width * 2 / 10) (height * 9 / 10 - 160);
  draw_string "'d' : change gradient type";
  moveto (width * 2 / 10) (height * 9 / 10 - 180);
  draw_string "'m' : open / close commands menu";
  moveto (width * 2 / 10) (height * 9 / 10 - 200);
  draw_string "'+' : add 10 points to the triangulation";
  moveto (width * 2 / 10) (height * 9 / 10 - 220);
  draw_string "'-' : remove 10 points to the triangulation";
  moveto (width * 2 / 10) (height * 9 / 10 - 240);
  draw_string "You can add a point by clicking inside the interface";
;;


(** Draw needed elements in the window, according to the environnement's
    datas.
    	- env : environnement.
 **)
let draw_env env = 
  if env.show_menu then
    draw_commands env
  else
    begin
      if env.show_filltriangle then
        begin
          fill_triangles (fill_color_function env) env.triangle_set
        end;

      if env.show_triangle then
        begin
          draw_triangles red env.triangle_set
        end;

      if env.show_circle then
        begin
          let circle_set = 
            Aset.map 
              env.triangle_set 
              Circle.circle_from_triangle 
              Circle.equals
          in
          draw_circles (rgb 191 191 191) circle_set
        end;

      if env.show_point then
        begin
          draw_points blue env.point_set
        end
    end;;

(* Environnement initialisation *)
(** Initialize and return an interface environnement with default
    values.
    	- add_point : 
    ((Triangle.triangle) Aset.aset -> Point.point 
    	-> (Triangle.triangle) Aset.aset),
    function used to add a point to a current triangulation.
    	- triangulation_function :
    ((Point.point) Aset.aset -> int -> int
    		-> (Triangle.triangle) Aset.aset),
    function used to compute a triangulation.
    	- nb_point : int, number of points used for the first
    triangulation.
    	- max_x : int, window width.
    	- max_y : int, window height.
 **)
let init_environnement add_point_function triangulation_function 
    nb_point max_x max_y = 

  Random.self_init ();

  let rgb_to = 
    (Random.float 255.0, Random.float 255.0, Random.float 255.0)
  and rgb_from = 
    (Random.float 255.0, Random.float 255.0, Random.float 255.0)
  in

  let fmax_x = 
    (float_of_int max_x)
  and fmax_y = 
    (float_of_int max_y)
  in

  let points = 
    Aset.fill_with_random
      (Aset.make Point.equals) 
      nb_point 
      (fun () -> Point.random 800 600)
  in

  let triangles = 
    triangulation_function points max_x max_y
  in

  {
    window_width = max_x;
    window_height = max_y;
    add_point_function = add_point_function;
    triangulation_function = triangulation_function;
    nb_point = nb_point;
    point_set = points;
    triangle_set = triangles;
    show_menu = true;
    show_point = true;
    show_triangle = true;
    show_circle = false;
    show_filltriangle = false;
    gradient_type = 0;
    colors = 
      (rgb_from, rgb_to);
    click = 
      Point.make (fmax_x /. 2.0) (fmax_y /. 2.0)
  };;

(** Generate a new triangulation with a new set of random points. The
    number of points depends of the environnement.
    	- env : environnement.
 **)
let random_triangulation env =
  let points = 
    Aset.fill_with_random	(Aset.make Point.equals) 
      env.nb_point 
      (fun () -> Point.random 800 600)
  in						
  let triangles = 
    env.triangulation_function	points 
      env.window_width 
      env.window_height 
  in
  env.triangle_set <- triangles;
  env.point_set <- points;;

(** Generate two random colors used in gradient functions.
    		- env : environnement.
 **)
let random_color env =
  Random.self_init ();
  let rgb_from = 
    (Random.float 255.0, Random.float 255.0, Random.float 255.0)
  and rgb_to = 
    (Random.float 255.0, Random.float 255.0, Random.float 255.0)
  in
  env.colors <- (rgb_from, rgb_to);;

(** Change the gradient type.
    		- env : environnement.
 **)
let next_gradient env =
  env.gradient_type <- ((env.gradient_type + 1) mod 3);;

(* Event gestion *)
(** Define an exception used to close the interface. **)
exception End;;

(** Define event loop for animation.
    Thanks to :
    https://caml.inria.fr/pub/docs/oreilly-book/html/book-ora050.html .
    	- f_init : (unit -> unit), function call at first to initialize
    the interface.
    	- f_end : (unit -> unit), function call by raising End
    exception.
    	- f_key : (char -> unit), function used to apply key event.
    	- f_mouse : (int -> int -> unit), function used to apply mouse
    event.
 **)
let skel f_init f_end f_key f_mouse = 
  f_init ();
  try 
    while true do 
      try 
        let s = 
          Graphics.wait_next_event 
            [Graphics.Button_down; Graphics.Key_pressed] 
        in 
        if s.Graphics.keypressed then f_key s.Graphics.key
        else 
        if s.Graphics.button then
          begin
            f_mouse s.Graphics.mouse_x s.Graphics.mouse_y
          end;
      with 
        End -> raise End
    done
  with 
    End  -> f_end ();;

(** Key handle event function. It will update the interface according
    to key event.
    	- drawfonction : (environnement -> unit), interface drawing
    function.
    	- env : environnement.
    	- c : char, char value of the key pressed.
 **)
let handle_key drawfonction env c = 
  (match c with
   |'c' -> raise End;
   |'e' -> env.show_circle <- not (env.show_circle);
   |'z' -> env.show_triangle <- not (env.show_triangle);
   |'a' -> env.show_point <- not (env.show_point);
   |'q' -> env.show_filltriangle <- not (env.show_filltriangle);
   |'r' -> random_triangulation env;
   |'s' -> random_color env;
   |'d' -> next_gradient env;
   |'m' -> env.show_menu <- not (env.show_menu);
   |'+' -> (
       env.nb_point <- env.nb_point + 10; 
       random_triangulation env);
   |'-' -> (
       env.nb_point <- (
         if (env.nb_point - 10) < 0 then 0 
         else (env.nb_point - 10)
       );
       random_triangulation env);
   | _ -> ());
  clear_graph ();
  drawfonction env;
  synchronize ();;

(** Mouse handle event function. It will update the interface according
    to mouse event.
    	- drawfonction : (environnement -> unit), interface drawing
    function.
    	- env : environnement.
    	- mouse_x : int, x coordinate of the mouse event.
    	- mouse_y : int, y coordinate of the mouse event.
 **)
let handle_mouse drawfonction env mouse_x mouse_y =
  if (
    not (env.show_menu) 
    && mouse_x > 0 
    && mouse_x < env.window_width && mouse_y > 0 
    && mouse_y < env.window_height
  ) then
    begin
      let point = 
        Point.make (float_of_int mouse_x) (float_of_int mouse_y)
      in
      env.point_set <- Aset.insert env.point_set point;
      env.triangle_set <- 
        env.add_point_function env.triangle_set point
    end;
  clear_graph ();
  drawfonction env;
  synchronize ();;
