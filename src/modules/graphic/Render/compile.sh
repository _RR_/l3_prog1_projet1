#!/bin/sh
set -x
MODULE=render

ocamlc -c -I ../../structure/Aset/ -I ../../structure/Counter/ -I ../../structure/Point/ -I ../../structure/Matrix/ -I ../../structure/Triangle/ -I ../../structure/Circle/ -I ../Draw/ -o $MODULE.cmi $MODULE.mli
ocamlc -c -I ../../structure/Aset/ -I ../../structure/Counter/ -I ../../structure/Point/ -I ../../structure/Matrix/ -I ../../structure/Triangle/ -I ../../structure/Circle/ -I ../Draw/ -o $MODULE.cmo $MODULE.ml
#ocamlc -a -o $MODULE.cma $MODULE.cmo

