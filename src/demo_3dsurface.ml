#directory "build/";;

#load "unix.cma";;
#load "graphics.cma";;
open Graphics;;

#load "structure.cma";;
#load "graphic.cma";;
open Draw;;

#use "triangulation.ml";;
#use "triangulation_ui.ml";;
#use "./addons/3dsurface/3dsurface.ml";;

let rec wait_g ()=
  let event = wait_next_event [Key_pressed] in
  if event.key == 'd' then 
    wait_g();;

let max_x = 800
and max_y = 600
and bonus_height = 50;;

let width = max_x
and height = max_y + bonus_height;;

let draw_text color text =
  set_color color;
  moveto (width / 50) (height - bonus_height/2);
  draw_string text;
  synchronize ();;

let col = (218.0, 255.0, 255.0)
and point = Point.make 10.0 10.0;;

let demo_3dsurface_angle () =
  clear_graph ();

  print_newline ();
  print_string (
    "Lighter color represent horizontal triangle "
    ^"while darker color represent vertical one."
  );
  print_newline ();
  print_string
    "\t Function : g(x, y) = 5 * (sin (x / 50 + y / 50)).";

  draw_text black (
    "Lighter color represent horizontal triangle "
    ^"while darker color represent vertical one."
    ^" g(x, y) = 5 * (sin (x / 50 + y / 50))."
  );

  let function_g x y = 
    5.0 *. (sin(x /. 50.0 +. y /. 50.0))
  in

  let mesh_g = 
    new_mesh point 600.0 25.0 function_g
  in

  let points_set = 
    grid_points_from mesh_g
  in

  let triangulation =
    clean_triangle_set (delaunay points_set 620 620) 620.0 620.0
  in

  let min_angle, max_angle =
    compute_extremum_declivity mesh_g triangulation
  in

  fill_triangles
    (fun tr ->
       triangle_color_declivity col mesh_g min_angle max_angle tr
    )
    triangulation;

  print_newline ();

  synchronize ();
  wait_g ();

  clear_graph ();

  let new_points_set = 
    smart_points_from mesh_g 2.0
  in

  draw_text black (
    "New set of points for the triangulation, more precise"
    ^" than before. Please wait, while it is processed."
  );

  fill_triangles
    (fun tr ->
       triangle_color_declivity col mesh_g min_angle max_angle tr
    )
    triangulation;

  draw_points blue new_points_set;
  synchronize ();


  let new_triangulation =
    clean_triangle_set (delaunay new_points_set 620 620) 620.0 620.0
  in

  clear_graph ();
  synchronize ();


  draw_text black (
    "Lighter color represent horizontal triangle "
    ^"while darker color represent vertical one."
    ^" g(x, y) = 5 * (sin (x / 50 + y / 50))."
  );

  fill_triangles
    (fun tr ->
       triangle_color_declivity col mesh_g min_angle max_angle tr
    )
    new_triangulation;

  synchronize ();
  wait_g ();;

let demo_3dsurface_height () =

  clear_graph ();
  synchronize ();

  let function_f x y = 
    (((x -. 300.0) /. 100.0)**(2.0)) +. (((y -. 300.0) /. 100.0)**(2.0))
  and function_g x y = 
    x /. (y +. 100.0)
  in

  let mesh_f = 
    new_mesh point 600.0 25.0 function_f
  and mesh_g =
    new_mesh point 600.0 25.0 function_g
  in

  let points_set = smart_points_from mesh_f 1.0 in

  let triangulation =
    clean_triangle_set (delaunay points_set 620 620) 620.0 620.0
  in

  let minh, maxh = 
    compute_extremum_height mesh_f triangulation
  in

  print_newline ();
  print_string (
    "Lighter color represent lower z value "
    ^"while darker color higher one."
  );
  print_newline ();
  print_string
    "\t Function : f(x, y) = (x/100 - 3)^2 + (y/100 - 3)^2.";

  draw_text black (
    "Lighter color represent lower z value "
    ^"while darker color represent higher one."
    ^" f(x, y) = (x/100 - 3)^2 + (y/100 - 3)^2.";
  );

  fill_triangles
    (fun t -> triangle_color_height col mesh_f minh maxh t)
    triangulation;

  synchronize ();
  wait_g ();
  clear_graph ();

  print_newline ();
  print_string
    "\t Function : g(x, y) = x / (y + 100).";

  draw_text black (
    "Lighter color represent lower z value "
    ^"while darker color represent higher one."
    ^" g(x, y) = x / (y + 100).";
  );

  let points_set_g = 
    smart_points_from mesh_g 0.5
  in

  let triangulation_g =
    clean_triangle_set (delaunay points_set_g 620 620) 620.0 620.0
  in

  let minh, maxh =
    compute_extremum_height mesh_g triangulation_g
  in

  fill_triangles
    (fun t -> triangle_color_height col mesh_g minh maxh t)
    triangulation_g;

  print_newline ();

  synchronize ();
  wait_g ();;

let demo () =

  open_graph
    (" "^(string_of_int width)^"x"^(string_of_int height)^"-0+0");

  set_window_title
    "Project-1 : addon 3d surface";

  auto_synchronize false;

  demo_3dsurface_angle ();

  demo_3dsurface_height ();

  close_graph ();;

demo ();;
