#directory "build/";;
#load "structure.cma";;

(** Return true if the two vertices (define by a couple of points) are
    equal, else false.
    	- v0 : (point * point).
    	- v1 : (point * point).
 **)
let vertice_equals v0 v1 =
  let p0, q0 = v0
  and p1, q1 = v1 in
  ((Point.equals p0 p1) && (Point.equals q0 q1))
  || ((Point.equals p0 q1) && (Point.equals q0 p1));;

(** Return the three vertices of a triangle.
    		- tr : triangle.
 **)
let generate_vertices tr =
  let p0, p1, p2 = 
    Triangle.get_points tr
  in
  let pmin = 
    Point.min_point p0 (Point.min_point p1 p2)
  and pmax = 
    Point.max_point p0 (Point.max_point p1 p2)
  and pmid = 
    Point.min_point
      (Point.max_point p0 (Point.min_point p1 p2))
      (Point.max_point p1 p2)
  in
  ((pmin, pmid), (pmin, pmax), (pmid, pmax));;

(** Return true if the vertice 'a' is in the list 'l', else false.
    	- a : (point * point), vertice to found.
    	- l : (point * point) list, vertice list.
 **)
let in_list a l =
  let rec in_list_aux l0 =
    match l0 with
    | [] -> false
    | t::q when (vertice_equals a t) -> true
    | _::q  -> in_list_aux q
  in in_list_aux l;;

(* compute convex border *)
(** Compute the convex border of a triangle set.
    	- triangle_set : triangle aset.
 **)
let border triangle_set =
  (** Return a list containing the vertices of all the triangle of the
      set.
      		- tr_set : triangle aset.
      		- v_list : (point * point) list, list used as an 
      accumulator.
   **)
  let rec triangles_to_verticelist tr_set v_list=
    if Aset.is_empty tr_set
    then
      (v_list)
    else
      begin
        let (tr_set1,tr) = 
          Aset.get_and_remove tr_set 
        in
        let v0,v1,v2 = 
          generate_vertices tr 
        in
        triangles_to_verticelist tr_set1 (v0::v1::v2::v_list)
      end
  in

  (** Return a list containing all the element of the list 'lst' which
      exists in only one occurrence.
      		- lst : (point * point) list.
   **)
  let rec remove_multi_occurences lst =
    match lst with
    | [] -> []
    | a::q ->
      if in_list a q then
        begin
          let v_not_equal x = 
            (not (vertice_equals x a))
          in
          remove_multi_occurences (List.filter v_not_equal q)
        end
      else
        a::(remove_multi_occurences q)
  in
  remove_multi_occurences (triangles_to_verticelist triangle_set []);;

(** Return a set of triangle where elements are made of a convex border 
    vertice and the point to add to the triangulation.
    	- triangle_set : triangle aset, set of all the triangle whose
    circumscribed circle contains the point 'point' to add.
    	- point : point, point to add to the triangulation.
 **)
let triangle_to_insert triangle_set point =
  (** Return the triangle defining by a vertice 'v' and the point to
      add 'point'.
      		- v : (point * point), a vertice.
   **)
  let create_triangle_from_vertice v =
    let p0, p1 = v in
    if (Triangle.is_counterclockwise p0 p1 point)
    then
      (Triangle.make p0 p1 point)
    else
      (Triangle.make p1 p0 point)
  in
  (** Return a triangle set, where each elements are made with a
      vertice of the convex border, and the point to add.
      		- v_list : (point * point) list, the vertice list containing
      all the vertices used in a triangle set.
      		- acc : triangle aset, variable used as accumulator,
      contains the new triangles.
   **)
  let rec create_triangle_from_vertices v_list acc =
    match v_list with
    | [] -> acc
    | v::q -> (
        create_triangle_from_vertices 
          q
          (Aset.insert acc (create_triangle_from_vertice v))
      )
  in
  let vertices = 
    border triangle_set 
  in
  create_triangle_from_vertices vertices (Aset.make Triangle.equals);;

(* Add the two basics triangles *)
(** Return a triangle set containing the initial configuration, two 
    bases triangles forming a rectangular area.
    	- max_x : int, triangulation width.
    	- max_y : int, triangulation height.
 **)
let generate_initial_config max_x max_y =
  let x = float_of_int max_x
  and y = float_of_int max_y
  and z = 0.0 in

  let p0 = Point.make z z
  and p1 = Point.make x z
  and p2 = Point.make z y
  and p3 = Point.make x y in

  let tr0 = Triangle.make p0 p1 p2
  and tr1 = Triangle.make p1 p3 p2 in

  (* Create the triangle set containing these two triangles. *)
  let tr_set0 = 
    Aset.make Triangle.equals 
  in
  let tr_set1 = 
    Aset.insert tr_set0 tr1
  in
  let tr_set2 = 
    Aset.insert tr_set1 tr0
  in
  tr_set2;;

(** Return a triangle set representing a triangulation.
    	- tr_set : triangle aset, state of the triangulation before 
    adding the point.
    	- p : point, point to add to the current triangulation.
 **)
let add_point tr_set p =
  (* Compute triangle whose circumscribed circle contains 'p'. *)
  let filter tr = 
    (Triangle.in_circle tr p)
  in
  let tr_to_remove =
    Aset.filter tr_set filter
  in
  (* Return the new triangles containing 'p'. *)
  let tr_set2 = 
    (triangle_to_insert tr_to_remove p)
  in
  (* Remove those triangle from the current triangle set. *)
  let tr_set3 =
    (Aset.diff tr_set tr_to_remove)
  in
  let tr_set4 = 
    (Aset.union tr_set3 tr_set2)
  in
  tr_set4;;

(** Return the couple of points set and triangles set of the current
    triangulation. This triangle set is the triangulation of the 
    point_set.
    	- point_set : point aset, points to add to the triangulation.
    	- tr_set : triangle aset, triangles of the triangulation.
 **)
let delaunay_step point_set tr_set =
  if Aset.is_empty point_set then
    (point_set, tr_set)
  else
    begin
      let point_set2, p = 
        Aset.get_and_remove point_set
      in
      (point_set2, add_point tr_set p)
    end;;

(* compute delaunay triangulation *)
(** Return the delaunay triangulation of a set of point in a restricted
    area, triangle set.
    	- points : point aset, points to triangulate.
    	- max_x : int, width of the triangulation area.
    	- max_y : int, height of the triangulation area.
 **)
let delaunay points max_x max_y =
  let rec delaunay_aux point_set tr_set=
    if Aset.is_empty point_set then
      tr_set
    else
      begin
        let next_pt, next_tr = 
          delaunay_step point_set tr_set
        in
        delaunay_aux next_pt next_tr
      end
  in
  delaunay_aux points (generate_initial_config max_x max_y);;
