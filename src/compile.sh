#!/bin/sh

# CHECK IF OCAMLC IS INSTALLED
echo "Checking that compile tools are presents..."
if command -v ocamlc;
then
    echo "OK";
else
    echo "ocamlc not found, cannot compile.";
    exit -1
fi;

set -x
mkdir -p build

echo "Starting compilation Now !"

# COMPILING MODULES IN CORRECT ORDER
for DIR in Counter Point Matrix Aset Triangle Circle
do
    echo "Entering $DIR"
    (cd ./modules/structure/"$DIR" && sh compile.sh)
    echo "Done"
done
for DIR in Draw Render
do
	echo "Entering $DIR"
	(cd ./modules/graphic/"$DIR"/ && sh compile.sh)
	echo "Done"
done
for DIR in '3dsurface'
do
	echo "Entering $DIR"
	(cd ./addons/"$DIR"/ && sh compile.sh)
	echo "Done"
done

# COPY ALL COMPILE FILES INTO BUILD FOLDER
cp ./modules/*/*/*.cmi ./build/
cp ./modules/*/*/*.cmo ./build/
cp ./addons/*/*.cmi ./build/
cp ./addons/*/*.cmo ./build/

# CREATING TWO CMA CONTAINING ALL LIBRARIES
#	structure.cma
#	graphic.cma
ocamlc -a -o ./build/structure.cma ./build/counter.cmo \
./build/point.cmo ./build/aset.cmo ./build/matrix.cmo \
./build/triangle.cmo ./build/circle.cmo

ocamlc -a -o ./build/graphic.cma ./build/draw.cmo ./build/render.cmo

# CREATING A CMA CONTAINING ADDON STRUCTURE LIBRARIES
#	3dsurface.cma
ocamlc -a -o ./build/3dsurface.cma ./build/mesh.cmo

rm ./modules/*/*/*.cmi
rm ./modules/*/*/*.cmo
rm ./addons/*/*.cmi
rm ./addons/*/*.cmo
rm ./build/*.cmo
