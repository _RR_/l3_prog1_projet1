#directory "build/";;

#load "unix.cma";;
#load "graphics.cma";;
open Graphics;;

#load "structure.cma";;
#load "graphic.cma";;
open Draw;;

(** Do a delaunay triangulation step by step, drawing each step.
    	- point_set : point aset, point set to use for the
    triangulation.
    	- max_x : int, width of the triangulation zone.
    	- max_y : int, height of the triangulation zone.
 **)
let delaunay_stepwise (point_set: Point.point Aset.aset) max_x max_y =
  let rec step_by_step pt_set tr_set =
    synchronize ();
    Unix.sleepf 0.8;
    clear_graph ();
    if Aset.is_empty pt_set then
      begin
        draw_triangles red tr_set;
        draw_points blue point_set;
      end
    else
      begin
        draw_triangles red tr_set;
        draw_points blue point_set;
        let ps, ts = 
          delaunay_step pt_set tr_set
        in
        step_by_step ps ts
      end
  in
  step_by_step point_set (generate_initial_config max_x max_y);;

(** Do a delaunay triangulation step by step, drawing each step with
    gradient color.
    	- point_set : point aset, point set to use for the
    triangulation.
    	- max_x : int, width of the triangulation zone.
    	- max_y : int, height of the triangulation zone.
    	- fill_fun : (triangle -> (int * int * int)), coloring function.
 **)
let delaunay_stepwise2 point_set max_x max_y fill_fun =
  let rec step_by_step pt_set tr_set =
    synchronize ();
    Unix.sleepf 0.3;
    clear_graph ();
    if Aset.is_empty pt_set then
      begin
        fill_triangles fill_fun tr_set;
      end
    else
      begin
        fill_triangles fill_fun tr_set;
        let ps, ts = 
          delaunay_step pt_set tr_set
        in
        step_by_step ps ts
      end
  in
  step_by_step point_set (generate_initial_config max_x max_y);;
