#directory "build/";;

#load "unix.cma";;
#load "graphics.cma";;
open Graphics;;

#load "structure.cma";;
#load "graphic.cma";;
open Draw;;

#use "triangulation.ml";;
#use "triangulation_ui.ml";;

let max_x = 800 
and max_y = 600 
and bonus_height = 50;;

let width = max_x
and height = max_y + bonus_height;;

let draw_text color text =
  set_color color;
  moveto (width / 50) (height - bonus_height/2);
  draw_string text;;

let rec wait_g ()=
  let event = 
    wait_next_event [Key_pressed]
  in
  if event.key == 'd' then 
    wait_g();;

let gen_random_point n =
  Aset.fill_with_random (Aset.make Point.equals)
    n
    (fun () -> Point.random 800 600);;

let demo_draw () =

  let p1 = Point.make 250.0 300.0
  and p2 = Point.make 700.0 300.0
  and p3 = Point.make 450.0 500.0 in

  let triangle = 
    Triangle.make p1 p2 p3
  in

  let circle = 
    Circle.circle_from_triangle triangle 
  in

  let points =
    Aset.insert 
      (Aset.insert 
         (Aset.insert 
            (Aset.make Point.equals) p1) 
         p2) 
      p3
  in

  print_newline ();
  print_string "Draw some points in black.";
  draw_text black "Draw some points in black.";

  draw_points black points;

  synchronize ();
  Unix.sleepf 1.5;
  clear_graph ();

  print_newline ();
  print_string "Draw a triangle in red made of those points.";
  draw_text black ("Draw a triangle in red made of those points.");

  draw_triangle red triangle;
  draw_points black points;

  synchronize ();
  Unix.sleepf 1.5;
  clear_graph ();

  print_newline();
  print_string "Draw the circumscribed circle of the triangle in grey.";
  draw_text black (
    "Draw the circumscribed circle of the triangle in grey."
    ^" Press enter to continue ..."
  );
  draw_triangle red triangle;
  draw_c (rgb 191 191 191) circle;
  draw_point cyan (Circle.coord_of_center circle);
  draw_points black points;

  print_newline();

  synchronize ();
  wait_g ();
  clear_graph ();;

let demo_triangulation () =
  clear_graph ();

  print_newline();
  print_string "Delaunay's triangulation of 10 points.";
  draw_text black (
    "Delaunay's triangulation of 10 points."
    ^" Press enter to continue ..."
  );

  let pt_set_init =
    gen_random_point 10
  in

  let tr_final = 
    delaunay pt_set_init max_x max_y
  in

  draw_triangles red tr_final;
  draw_points blue pt_set_init;

  print_newline();
  print_string "Delaunay's triangulation of 100 points.";


  synchronize ();
  wait_g();
  clear_graph ();

  draw_text black (
    "Delaunay's triangulation of 100 points."
    ^" Press enter to continue ..."
  );
  let pt_set_init =
    gen_random_point 100 
  in

  let tr_final = 
    delaunay pt_set_init max_x max_y
  in

  draw_triangles red tr_final;
  draw_points blue pt_set_init;

  print_newline ();

  synchronize ();
  wait_g ();;

let demo_step () =
  clear_graph ();

  print_newline();
  print_string "Delaunay's triangulation step-by-step of 2 points.";

  let pt_set_init =
    gen_random_point 2
  in

  delaunay_stepwise pt_set_init max_x max_y;

  draw_points blue pt_set_init;
  draw_text black (
    "Delaunay's triangulation step-by-step of 2 points."
    ^" Press enter to continue ..."
  );

  print_newline();
  print_string
    "Delaunay's triangulation step-by-step of 10 points.";

  synchronize ();
  wait_g();
  clear_graph ();

  let pt_set_init =
    gen_random_point 10
  in

  delaunay_stepwise pt_set_init max_x max_y;
  draw_points blue pt_set_init;
  draw_text black (
    "Delaunay's triangulation step-by-step of 10 points."
    ^" Press enter to continue ..."
  );
  print_newline();

  synchronize ();
  wait_g();;

let demo_color_gradient () =
  clear_graph ();

  print_newline();
  print_string (
    "Delaunay's triangulation of 100 points"
    ^" with gradient colors."
  );

  let pt_set_init = 
    gen_random_point 100 
  in

  let tr_final = 
    delaunay pt_set_init max_x max_y
  in

  let fmax_x = float_of_int max_x
  and fmax_y = float_of_int max_y in

  let rgb_from = (0.0, 120.0, 123.0)
  and rgb_to = (188.0, 227.0, 228.0) in

  print_newline ();
  print_string "\tWith a gradient on x axis.";

  draw_text black (
    "Delaunay's triangulation of 100 points"
    ^" with gradient colors : on X axis."
    ^" Press enter to continue ..."
  );

  fill_triangles 
    (fun p -> (gradient_x rgb_from rgb_to fmax_x p))
    tr_final;

  print_newline ();
  print_string "\tWith a gradient on y axis.";

  synchronize ();
  wait_g ();
  clear_graph ();
  draw_text black (
    "Delaunay's triangulation of 100 points"
    ^" with gradient colors : on Y axis."
    ^" Press enter to continue ..."
  );

  fill_triangles 
    (fun p -> (gradient_y rgb_from rgb_to fmax_y p))
    tr_final;

  print_newline ();
  print_string "\tWith a circular gradient.";


  synchronize ();
  wait_g ();
  clear_graph ();
  draw_text black (
    "Delaunay's triangulation of 100 points"
    ^" with gradient colors : circular around (400, 300)."
    ^" Press enter to continue ..."
  );
  let center =
    Point.make 400.0 300.0
  in

  fill_triangles
    (fun p -> (
         gradient_circle rgb_from rgb_to 900.0 center p)
    )
    tr_final;
  print_newline();

  synchronize ();
  wait_g ();;

let demo_color_gradient_step () =
  clear_graph();

  print_newline();
  print_string (
    "Delaunay's triangulation of 20 points"
    ^ " step-by-step with gradient colors."
  );

  let pt_set_init = gen_random_point 20 in
  Random.self_init();

  let fmax_x = float_of_int max_x in

  let rgb_from = (0.0, 120.0, 123.0)
  and rgb_to = (188.0, 227.0, 228.0) in

  let fill_fun p = 
    (gradient_x rgb_from rgb_to fmax_x p)
  in

  delaunay_stepwise2 pt_set_init max_x max_y fill_fun;

  draw_text black (
    "Delaunay's triangulation of 20 points"
    ^ " step-by-step with gradient colors: X axis."
    ^" Press enter to continue ..."
  );

  print_newline();
  synchronize ();
  wait_g ();;

let demo () =
  close_graph ();
  open_graph (
    " " ^ string_of_int width ^ "x"
    ^ string_of_int height ^ "-0+0"
  );

  auto_synchronize false;

  set_window_title
    "Project-1 : Delaunay's triangulation demonstration";

  demo_draw ();
  demo_triangulation ();
  demo_step ();

  demo_color_gradient ();
  demo_color_gradient_step ();

  close_graph ();;

demo ();;
