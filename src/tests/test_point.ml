#directory "build/";;
#directory "../build";;

#load "structure.cma";;
open Point;;

let max = 100.0;;

Random.self_init();;

let test_int_coord_of n = 
  (* TEST int_coord_of *)
  print_string "TEST int_coord_of : ";
  for i = 0 to n do
    let px = Random.float max 
    and py = Random.float max in
    
    let p = make px py in
    
    assert (
      let coord = 
        int_coord_of p
      in
      (int_of_float px) = (fst coord) &&
      (int_of_float py) = (snd coord)
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_float_coord_of n = 
  (* TEST float_coord_of *)
  print_string "TEST float_coord_of : ";
  for i = 0 to n do
    let px = Random.float max
    and py = Random.float max in
    
    let p = make px py in
    
    assert (
      let coord = 
        float_coord_of p 
      in
      px = (fst coord) &&
      py = (snd coord)
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_invert n = 
  (* TEST invert *)
  print_string "TEST invert : ";
  for i = 0 to n do
    let px = Random.float max
    and py = Random.float max in
    
    let q = invert (make px py) in
    
    assert (
      let coord = 
        float_coord_of q
      in
      px = (0.0 -. (fst coord)) &&
      py = (0.0 -. (snd coord))
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_add n = 
  (* TEST add *)
  print_string "TEST add : ";
  for i = 0 to n do
    let px = Random.float max 
    and py = Random.float max 
    and qx = Random.float max
    and qy = Random.float max in
    
    let p = make px py 
    and q = make qx qy in
    
    let add_p_q = add p q in
    
    assert (
      let coord = 
        float_coord_of add_p_q
      in
      (px +. qx) = (fst coord) &&
      (py +. qy) = (snd coord)
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_sub n = 
  (* TEST sub *)
  print_string "TEST sub : ";
  for i = 0 to n do
    let px = Random.float max
    and py = Random.float max 
    and qx = Random.float max 
    and qy = Random.float max in
    
    let p = make px py 
    and q = make qx qy in
    
    let sub_p_q = sub p q in
    
    assert (
      let coord = 
        float_coord_of sub_p_q
      in
      (px -. qx) = (fst coord) &&
      (py -. qy) = (snd coord)
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_times_scal n = 
  (* TEST times_scal *)
  print_string "TEST times_scal : ";
  for i = 0 to n do
    let px = Random.float max
    and py = Random.float max 
    and k = Random.float max in
    
    let p = make px py in
    
    let q = times_scal p k in
    
    assert (
      let coord =
        float_coord_of q
      in
      (px *. k) = (fst coord) &&
      (py *. k) = (snd coord)
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_div_scal n = 
  (* TEST div_scal *)
  print_string "TEST div_scal : ";
  for i = 0 to n do
    let px = Random.float max 
    and py = Random.float max 
    and k = Random.float max in
    
    let p = make px py in
    
    let q = div_scal p k in
    
    assert (
      let coord = 
        float_coord_of q
      in
      (px /. k) = (fst coord) &&
      (py /. k) = (snd coord)
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_equals n =
  (* TEST equals *)
  print_string "TEST equals : ";
  for i = 0 to n do
    let px = Random.float max 
    and py = Random.float max 
    and qx = Random.float max 
    and qy = Random.float max in
    
    let p = make px py 
    and q = make qx qy in
    
    assert (equals p p);
    assert ((equals p q) = ((px = qx) && (py = qy)));
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_point n = 
  test_int_coord_of n;
  test_float_coord_of n;
  test_invert n;
  test_add n;
  test_sub n;
  test_times_scal n;
  test_div_scal n;
  test_equals n;;

test_point 10;;
