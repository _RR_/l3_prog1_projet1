#directory "build/";;
#directory "../build";;

#load "structure.cma";;

let max = 100;;

Random.self_init();;

let test_get_points n = 
  (* TEST get_points *)
  print_string "TEST get_points : ";
  for i = 0 to n do
    let p1 = Point.random max max 
    and p2 = Point.random max max
    and p3 = Point.random max max 
    in
    let t = 
      Triangle.make p1 p2 p3
    in
    assert (
      let x, y, z = 
        Triangle.get_points t
      in
      (Point.equals x p1) &&
      (Point.equals y p2) &&
      (Point.equals z p3)
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_equals n =
  (* TEST equals *)
  print_string "TEST equals : ";
  for i = 0 to n do
    let p1 = Point.random max max 
    and p2 = Point.random max max
    and p3 = Point.random max max
    and q1 = Point.random max max 
    and q2 = Point.random max max
    and q3 = Point.random max max
    in
    let t1 = 
      Triangle.make p1 p2 p3 
    and t2 = 
      Triangle.make q1 q2 q3 
    and t3 = 
      Triangle.make p3 p2 p1
    in
    assert (Triangle.equals t1 t1);
    assert (Triangle.equals t1 t3);
    assert (
      (Triangle.equals t1 t2) =
      ((Point.equals q1 p1) &&
       (Point.equals q2 p2) &&
       (Point.equals q3 p3))
    );
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_is_counterclockwise () = 
  (* TEST is_counterclockwise *)
  print_string "TEST test_is_counterclockwise : ";
  let p1 = Point.make 200.0 50.0 
  and p2 = Point.make 100.0 100.0
  and p3 = Point.make 10.0 10.0
  in
  assert (Triangle.is_counterclockwise p1 p2 p3);
  assert ((Triangle.is_counterclockwise p3 p2 p1) = false);
  print_string ".";
  print_string "OK";
  print_newline ();;

let test_in_circle () =
  (* TEST in_circle *)
  print_string "TEST test_in_circle : ";
  let p1 = Point.make 100.0 0.0 
  and p2 = Point.make 50.0 50.0
  and p3 = Point.make 00.0 0.0
  and not_in = Point.make 900.0 15.0
  and inside = Point.make 25.0 25.0
  in
  let t = 
    Triangle.make p1 p2 p3
  in
  assert ((Triangle.in_circle t not_in) = false);
  assert (Triangle.in_circle t inside);
  print_string ".";
  print_string "OK";
  print_newline ();;

let test_triangle n = 
  test_get_points n;
  test_equals n;
  test_is_counterclockwise ();
  test_in_circle ();;

test_triangle 10;;
