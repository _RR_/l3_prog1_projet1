#directory ".";;
#directory "build/";;

#load "unix.cma";;
#load "graphics.cma";;
open Graphics;;

#load "structure.cma";;
#load "graphic.cma";;
open Draw;;

#use "triangulation.ml";;

#load "unix.cma";;

let max_x = 800 and max_y = 600;;

let wait() =
  print_newline();
  prerr_string "Press enter to continue ...";
  prerr_newline ();
  if (read_line() = "") then ();;

let gen_random_point n =
  Aset.fill_with_random	
    (Aset.make Point.equals) 
    n 
    (fun () -> Point.random 800 600);;

let coefficient c = 
  if (c >= 0.0 && c <= 1.0) then c
  else
    if (c < 0.0) then 0.0
    else 1.0;;

let test_color () =

  let pt_set_init = 
    gen_random_point 100 
  in
  let tr_final = 
    delaunay pt_set_init max_x max_y
  in

  let fmax_x = 
    float_of_int max_x
  and fmax_y = 
    float_of_int max_y 
  in

  let rgb_from = 
    (Random.float 255.0, Random.float 255.0, Random.float 255.0)
  and rgb_to = 
    (Random.float 255.0, Random.float 255.0, Random.float 255.0)
  in

  print_string "Gradient on X axis.";
  fill_triangles 
    (fun p -> (gradient_x rgb_from rgb_to fmax_x p)) 
    tr_final;
  wait ();

  print_string "Gradient on Y axis.";
  fill_triangles 
    (fun p -> (gradient_y rgb_from rgb_to fmax_y p))
    tr_final;
  wait ();

  print_string "Circular gradient centring on point (400, 300).";
  let center = 
    Point.make 400.0 300.0
  in
  fill_triangles 
    (fun t -> gradient_circle rgb_from rgb_to 800.0 center t) 
    tr_final;

  wait();;

let test () =
  close_graph ();
  open_graph 
    (" "^(string_of_int max_x)^"x"^(string_of_int max_y)^"-0+0");

  test_color ();

  clear_graph ();
  close_graph ();;

test ();;
