#directory ".";;
#directory "build/";;

#load "unix.cma";;
#load "graphics.cma";;
open Graphics;;

#load "structure.cma";;
#load "graphic.cma";;
open Draw;;

let max = 100.0;;

Random.self_init();;

let wait() =
  print_newline();
  prerr_string "Press enter to continue ...";
  prerr_newline ();
  if (read_line() = "") then ();;

let test_coord_of_center n = 
  (* TEST coord_of_center *)
  print_string "TEST coord_of_center : ";
  for i = 0 to n do
    let px = 
      Random.float max 
    and py = 
      Random.float max 
    and radius = 
      Random.float max 
    in
    
    let p = 
      Point.make px py
    in
    
    let circle = 
      Circle.make p radius
    in
    
    assert (Point.equals p (Circle.coord_of_center circle));
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_get_radius n = 
  (* TEST get_radius *)
  print_string "TEST get_radius : ";
  for i = 0 to n do
    let px = 
      Random.float max 
    and py = 
      Random.float max 
    and radius = 
      Random.float max 
    in
    
    let circle = 
      Circle.make (Point.make px py) radius
    in
    
    assert ((Circle.get_radius circle) = radius);
    print_string ".";
  done;
  print_string "OK";
  print_newline ();;

let test_circle_from_triangle () =
  (* TEST circle_from_triangle *)
  print_string "\nDraw a circumscribed circle.";
  
  open_graph " 800x600-0+0";
  let p1 = 
    Point.random 800 600
  and p2 = 
    Point.random 800 600
  and p3 = 
    Point.random 800 600 in
    
  let triangle = 
    Triangle.make p1 p2 p3 
  in

  let circle =
    Circle.circle_from_triangle triangle 
  in

  let triangles = 
    Aset.insert (Aset.make Triangle.equals) triangle
  and circles = 
    Aset.insert (Aset.make Circle.equals) circle
  in

  draw_triangles red triangles;
  draw_circles green circles;
  wait ();
  close_graph ();;

let test_circle n = 
  test_coord_of_center n;
  test_get_radius n;
  test_circle_from_triangle ();;

test_circle 10;;
