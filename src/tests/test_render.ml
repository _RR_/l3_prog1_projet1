#directory ".";;
#directory "build/";;

#load "graphics.cma";;
open Graphics;;

#load "structure.cma";;
#load "graphic.cma";;
open Draw;;
open Render;;

#use "triangulation.ml";;
#use "triangulation_ui.ml";;

let max_x = 800
and max_y = 600;;

Random.self_init ();;

let r = Random.int 255
and g = Random.int 255
and b = Random.int 255;;

let environnement = 
  init_environnement (add_point) (delaunay) 100 max_x max_y;;

skel (fun () -> init_screen max_x max_y; 
       draw_env environnement; 
       synchronize ())
  (fun () -> close_screen ())
  (handle_key draw_env environnement)
  (handle_mouse draw_env environnement);;
