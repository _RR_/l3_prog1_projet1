#directory "build/";;
#directory "../build";;

#load "structure.cma";;

Random.self_init();;

let test_det () =
  print_string "Test matrix : ";
  
  Random.self_init();
  
  let m = Matrix.make 4 0.0 in
  Matrix.set m 0 0 1.0;
  Matrix.set m 0 1 2.0;
  Matrix.set m 0 2 3.0;
  Matrix.set m 0 3 4.0;
  Matrix.set m 1 0 5.0;
  Matrix.set m 1 1 6.0;
  Matrix.set m 1 2 7.0;
  Matrix.set m 1 3 8.0;
  Matrix.set m 2 0 9.0;
  Matrix.set m 2 1 10.0;
  Matrix.set m 2 2 11.0;
  Matrix.set m 2 3 12.0;
  Matrix.set m 3 0 13.0;
  Matrix.set m 3 1 14.0;
  Matrix.set m 3 2 15.0;
  Matrix.set m 3 3 16.0;
  
  assert ((Matrix.det m) = 0.0);
  print_char '.';
  
  let m1 = Matrix.make 2 0.0 in
  
  let a = (Random.float 100.0)
  and b = (Random.float 100.0)
  and c = (Random.float 100.0)
  and d = (Random.float 100.0) in
  
  Matrix.set m1 0 0 a;
  Matrix.set m1 0 1 b;
  Matrix.set m1 1 0 c;
  Matrix.set m1 1 1 d;
  
  assert ((Matrix.det m1) = (a *. d -. b *. c));
  print_char '.';
  print_string "OK\n";;

test_det ();;
