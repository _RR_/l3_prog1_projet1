#directory "build/";;
#directory "../build";;

#load "structure.cma";;
open Aset;;

let f x y = x = y;;

let set1 = 
  insert 
    (insert 
      (insert 
        (insert 
          (insert (make f) 5) 
          7) 
        3) 
      2) 
    0;;
    
let set2 = 
  insert 
    (insert 
      (insert
        (insert 
          (insert (make f) 1) 
          2) 
        3) 
      4) 
    5;;

print_string "\nset1 : ";;
print set1 print_int;;
print_string "should be \"0 2 3 7 5\"\n";;

print_string "\nset2 : ";;
print set2 print_int;;
print_string "should be \"5 4 3 2 1\"\n";;

print_string "\nunion : ";;
print (union set1 set2) print_int;;
print_string "should be \" 0 2 3 7 5 4 1 \"\n";;

print_string "\ninter : ";;
print (inter set1 set2) print_int;;
print_string "should be \" 2 3 5 \"\n";;

print_string "\ndiff : ";;
print (diff set1 set2) print_int;;
print_string "should be \" 0 7 \"\n";;

print_string "\nremove 7: ";;
print (remove set1 7) print_int;;
print_string "should be \" 0 2 3 5 \"\n";;

print_string "\nget_and_remove : ";;
let sset = get_and_remove set1 ;;
print_int (snd sset);;
print_string " - ";;
print (fst sset) print_int;;
print_string "should be \" 0 - 2 3 5 7 \"\n";;

print_string "\nmap float_of_int : ";;
print (map set1 float_of_int (=)) print_float;;
print_string "should be \" 0. 2. 3. 7. 5. \"\n";;

print_string "\nfilter : ";;
print (filter set1 (function x -> x>2)) print_int;;
print_string "should be \" 3 5 7 \"\n";;

Random.self_init();;
let ran () = (Random.int 100);;

print_string "\nfill_with_randoms : ";;
print (fill_with_random set1 100 ran) print_int;;
print_newline();

print_string "additional tests : ";;
assert( length (union set1 set2) == 7);;
print_string ".";;
assert( length (inter set1 set2) == 3);;
print_string ".";;
assert( length (make (=)) == 0);;
print_string ".";;
assert( length (diff set1 set2) == 2);;
print_string ".";;
assert( length (remove set1 3) == 4);;
print_string ".";;
print_string "OK\n";;
