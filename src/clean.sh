#!/bin/sh

set -x
rm ./build/*.cma
rm ./build/*.cmi
rm ./build/*.cmo
rm ./addons/*/*.cmi
rm ./addons/*/*.cmo
rm ./modules/*/*/*.cmi
rm ./modules/*/*/*.cmo
exit 0
