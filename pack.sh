#!/bin/sh

echo "Packing code..."
FILENAME="LosekootPiauThuillier"
set -x
git archive --format zip --prefix=$FILENAME/ --output $FILENAME.zip master
zip --delete $FILENAME.zip "$FILENAME/rapport/*"
zip --delete $FILENAME.zip "$FILENAME/diapo/*"
zip --delete $FILENAME.zip "$FILENAME/src/addons/guibas_stolfi/*"
zip --delete $FILENAME.zip "$FILENAME/src/modules/structure/Counter/R/*"
zip --delete $FILENAME.zip "$FILENAME/src/modules/structure/Counter/use_counter.ml"
